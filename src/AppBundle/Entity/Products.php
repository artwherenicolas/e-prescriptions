<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Products
 *
 * @ORM\Table(name="products")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductsRepository")
 */
class Products
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ean", type="string", length=255, nullable=false)
     */
    private $ean;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=252, nullable=false)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    private $date;
	
	/**
     * @var status
     * @ORM\Column(name="status", type="boolean",options={"default":true})
     */
    protected $status;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ProductsToPrescriptions", mappedBy="product")
     */
    private $productsToDescription;
	
	
	/**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\UserToProducts", mappedBy="product")
	*/
    private $productsToUser;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ean
     *
     * @param string $ean
     *
     * @return Products
     */
    public function setEan($ean)
    {
        $this->ean = $ean;

        return $this;
    }

    /**
     * Get ean
     *
     * @return string
     */
    public function getEan()
    {
        return $this->ean;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Products
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Products
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
		$this->setDate(new \DateTime());
        $this->productsToDescription = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add productsToDescription
     *
     * @param \AppBundle\Entity\ProductsToPrescriptions $productsToDescription
     *
     * @return Products
     */
    public function addProductsToDescription(\AppBundle\Entity\ProductsToPrescriptions $productsToDescription)
    {
        $this->productsToDescription[] = $productsToDescription;

        return $this;
    }

    /**
     * Remove productsToDescription
     *
     * @param \AppBundle\Entity\ProductsToPrescriptions $productsToDescription
     */
    public function removeProductsToDescription(\AppBundle\Entity\ProductsToPrescriptions $productsToDescription)
    {
        $this->productsToDescription->removeElement($productsToDescription);
    }

    /**
     * Get productsToDescription
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductsToDescription()
    {
        return $this->productsToDescription;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return Products
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Add productsToUser
     *
     * @param \AppBundle\Entity\UserToProducts $productsToUser
     *
     * @return Products
     */
    public function addProductsToUser(\AppBundle\Entity\UserToProducts $productsToUser)
    {
        $this->productsToUser[] = $productsToUser;

        return $this;
    }

    /**
     * Remove productsToUser
     *
     * @param \AppBundle\Entity\UserToProducts $productsToUser
     */
    public function removeProductsToUser(\AppBundle\Entity\UserToProducts $productsToUser)
    {
        $this->productsToUser->removeElement($productsToUser);
    }

    /**
     * Get productsToUser
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductsToUser()
    {
        return $this->productsToUser;
    }
	
	public function __toString() {
		return $this->name;
	}
}
