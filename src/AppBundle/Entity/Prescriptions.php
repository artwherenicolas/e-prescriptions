<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Prescriptions
 *
 * @ORM\Table(name="prescriptions", indexes={@ORM\Index(name="addresse", columns={"address"}), @ORM\Index(name="doctor", columns={"doctor"}), @ORM\Index(name="representative", columns={"representative"})})
 * @ORM\Entity
 */
class Prescriptions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

     /**
      * @ORM\Column(type="text")
     */
     private $signature;
	 
	/**
      * @ORM\Column(type="text",nullable=true)
     */
     private $service;

    /**
     * @ORM\Column(type="text")
     */
    private $schedule;
	
	/**
     * @ORM\Column(type="integer")
     */
    private $deliverymode=0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    private $date;

    /**
     * @var \DoctorsAddress
     *
     * @ORM\ManyToOne(targetEntity="DoctorsAddress")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="address", referencedColumnName="id")
     * })
     */
    private $address;


    /**
     * @var \Doctors
     *
     * @ORM\ManyToOne(targetEntity="Doctors")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="doctor", referencedColumnName="id")
     * })
     */
    private $doctor;

    /**
     * @var \FosUser
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="representative", referencedColumnName="id")
     * })
     */
    private $representative;

    /**
     * @ORM\OneToMany(targetEntity="ProductsToPrescriptions", mappedBy="prescription",cascade={"persist", "remove"})
     */
    private $productToPrescriptions;

 
 
    /**
     * @var \addressDelivery
     *
     * @ORM\OneToOne(targetEntity="PrescriptionAddress")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="addressDelivery", referencedColumnName="id")
     * })
     */
    private $addressDelivery;
	
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->productToPrescriptions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set signature
     *
     * @param string $signature
     *
     * @return Prescriptions
     */
    public function setSignature($signature)
    {
        $this->signature = $signature;

        return $this;
    }

    /**
     * Get signature
     *
     * @return string
     */
    public function getSignature()
    {
        return $this->signature;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Prescriptions
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set schedule
     *
     * @param string $schedule
     *
     * @return Prescriptions
     */
    public function setSchedule($schedule)
    {
        $this->schedule = $schedule;

        return $this;
    }

    /**
     * Get schedule
     *
     * @return string
     */
    public function getSchedule()
    {
        return $this->schedule;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Prescriptions
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set address
     *
     * @param \AppBundle\Entity\DoctorsAddress $address
     *
     * @return Prescriptions
     */
    public function setAddress(\AppBundle\Entity\DoctorsAddress $address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return \AppBundle\Entity\DoctorsAddress
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set doctor
     *
     * @param \AppBundle\Entity\Doctors $doctor
     *
     * @return Prescriptions
     */
    public function setDoctor(\AppBundle\Entity\Doctors $doctor = null)
    {
        $this->doctor = $doctor;

        return $this;
    }

    /**
     * Get doctor
     *
     * @return \AppBundle\Entity\Doctors
     */
    public function getDoctor()
    {
        return $this->doctor;
    }

    /**
     * Set representative
     *
     * @param \AppBundle\Entity\User $representative
     *
     * @return Prescriptions
     */
    public function setRepresentative(\AppBundle\Entity\User $representative = null)
    {
        $this->representative = $representative;

        return $this;
    }

    /**
     * Get representative
     *
     * @return \AppBundle\Entity\User
     */
    public function getRepresentative()
    {
        return $this->representative;
    }

    /**
     * Add productToPrescription
     *
     * @param \AppBundle\Entity\ProductsToPrescriptions $productToPrescription
     *
     * @return Prescriptions
     */
    public function addProductToPrescription(\AppBundle\Entity\ProductsToPrescriptions $productToPrescription)
    {
        $this->productToPrescriptions[] = $productToPrescription;

        return $this;
    }

    /**
     * Remove productToPrescription
     *
     * @param \AppBundle\Entity\ProductsToPrescriptions $productToPrescription
     */
    public function removeProductToPrescription(\AppBundle\Entity\ProductsToPrescriptions $productToPrescription)
    {
        $this->productToPrescriptions->removeElement($productToPrescription);
    }

    /**
     * Get productToPrescriptions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductToPrescriptions()
    {
        return $this->productToPrescriptions;
    }

    /**
     * Set addressDelivery
     *
     * @param \AppBundle\Entity\PrescriptionAddress $addressDelivery
     *
     * @return Prescriptions
     */
    public function setAddressDelivery(\AppBundle\Entity\PrescriptionAddress $addressDelivery = null)
    {
        $this->addressDelivery = $addressDelivery;

        return $this;
    }

    /**
     * Get addressDelivery
     *
     * @return \AppBundle\Entity\PrescriptionAddress
     */
    public function getAddressDelivery()
    {
        return $this->addressDelivery;
    }

    /**
     * Set deliveryMode
     *
     * @param integer $deliveryMode
     *
     * @return Prescriptions
     */
    public function setDeliveryMode($deliveryMode)
    {
        $this->deliveryMode = $deliveryMode;

        return $this;
    }

    /**
     * Get deliveryMode
     *
     * @return integer
     */
    public function getDeliveryMode()
    {
        return $this->deliveryMode;
    }

    /**
     * Set service
     *
     * @param string $service
     *
     * @return Prescriptions
     */
    public function setService($service)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return string
     */
    public function getService()
    {
        return $this->service;
    }
}
