<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $representativeId;
    
	/**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\UserToProducts", mappedBy="product")
	*/
    private $userToProduct;

    /**
     * Set representativeId
     *
     * @param integer $representativeId
     *
     * @return User
     */
    public function setRepresentativeId($representativeId)
    {
        $this->representativeId = $representativeId;

        return $this;
    }

    /**
     * Get representativeId
     *
     * @return integer
     */
    public function getRepresentativeId()
    {
        return $this->representativeId;
    }

    /**
     * Add userToProduct
     *
     * @param \AppBundle\Entity\UserToProducts $userToProduct
     *
     * @return User
     */
    public function addUserToProduct(\AppBundle\Entity\UserToProducts $userToProduct)
    {
        $this->userToProduct[] = $userToProduct;

        return $this;
    }

    /**
     * Remove userToProduct
     *
     * @param \AppBundle\Entity\UserToProducts $userToProduct
     */
    public function removeUserToProduct(\AppBundle\Entity\UserToProducts $userToProduct)
    {
        $this->userToProduct->removeElement($userToProduct);
    }

    /**
     * Get userToProduct
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserToProduct()
    {
        return $this->userToProduct;
    }
}
