<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ambta\DoctrineEncryptBundle\Configuration\Encrypted;

/**
 * Doctors
 *
 * @ORM\Table(name="doctors")
 * @ORM\Entity
 *
 */
class Doctors
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="firstname", type="string", length=255, nullable=false)
     */
    private $firstname;

    /**
     * @var string
     * @ORM\Column(name="lastname", type="string", length=255, nullable=false)
     */
    private $lastname;

    /**
     * @var string
     * @ORM\Column(name="hospital", type="string", length=255, nullable=true)
     */
    private $hospital;


    /**
     * @var string
     * @ORM\Column(name="onekey", type="string", length=255, nullable=true)
     */
    private $onekey;

    /**
     * @var string
     * @ORM\Column(name="inami", type="string", length=255, nullable=false)
     */
    private $inami;


    /**
     * @ORM\OneToMany(targetEntity="DoctorsAddress", mappedBy="doctor", cascade="all", orphanRemoval=true)
     * @ORM\OrderBy({"id" = "DESC"})
     */
    private $address;

	/**
     * @var status
     * @ORM\Column(name="status", type="boolean",options={"default":true})
     */
    protected $status;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return Doctors
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return Doctors
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set iname
     *
     * @param string $iname
     *
     * @return Doctors
     */
    public function setIname($iname)
    {
        $this->iname = $iname;

        return $this;
    }

    /**
     * Get iname
     *
     * @return string
     */
    public function getIname()
    {
        return $this->iname;
    }

    /**
     * Set inami
     *
     * @param string $inami
     *
     * @return Doctors
     */
    public function setInami($inami)
    {
        $this->inami = $inami;

        return $this;
    }

    /**
     * Get inami
     *
     * @return string
     */
    public function getInami()
    {
        return $this->inami;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->address = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add address
     *
     * @param \AppBundle\Entity\DoctorsAddress $address
     *
     * @return Doctors
     */
    public function addAddress(\AppBundle\Entity\DoctorsAddress $address)
    {
        $this->address[] = $address;

        return $this;
    }

    /**
     * Remove address
     *
     * @param \AppBundle\Entity\DoctorsAddress $address
     */
    public function removeAddress(\AppBundle\Entity\DoctorsAddress $address)
    {
        $this->address->removeElement($address);
    }

    /**
     * Get address
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set hospital
     *
     * @param string $hospital
     *
     * @return Doctors
     */
    public function setHospital($hospital)
    {
        $this->hospital = $hospital;

        return $this;
    }

    /**
     * Get hospital
     *
     * @return string
     */
    public function getHospital()
    {
        return $this->hospital;
    }

    /**
     * Set onekey
     *
     * @param string $onekey
     *
     * @return Doctors
     */
    public function setOnekey($onekey)
    {
        $this->onekey = $onekey;

        return $this;
    }

    /**
     * Get onekey
     *
     * @return string
     */
    public function getOnekey()
    {
        return $this->onekey;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return Doctors
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }
}
