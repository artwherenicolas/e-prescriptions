<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DoctorsAddress
 *
 * @ORM\Table(name="prescription_address", indexes={@ORM\Index(name="prescription", columns={"id"})})
 * @ORM\Entity
 */
class PrescriptionAddress
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="text", length=65535, nullable=false)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=255, nullable=false)
     */
    private $number;

    /**
     * @var string
     *
     * @ORM\Column(name="zip", type="string", length=255, nullable=false)
     */
    private $zip;

    /**
     * @var string
     *
     * @ORM\Column(name="box", type="string", length=255, nullable=true)
     */
    private $box;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=false)
     */
    private $city;

	/**
     * @ORM\OneToOne(targetEntity="Prescriptions", inversedBy="addressDelivery")
     * @ORM\JoinColumn(name="Prescription", referencedColumnName="id")
     */
    private $prescription;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return PrescriptionAddress
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set number
     *
     * @param string $number
     *
     * @return PrescriptionAddress
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set zip
     *
     * @param string $zip
     *
     * @return PrescriptionAddress
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * Get zip
     *
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set box
     *
     * @param string $box
     *
     * @return PrescriptionAddress
     */
    public function setBox($box)
    {
        $this->box = $box;

        return $this;
    }

    /**
     * Get box
     *
     * @return string
     */
    public function getBox()
    {
        return $this->box;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return PrescriptionAddress
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set prescription
     *
     * @param \AppBundle\Entity\Prescriptions $prescription
     *
     * @return PrescriptionAddress
     */
    public function setPrescription(\AppBundle\Entity\Prescriptions $prescription = null)
    {
        $this->prescription = $prescription;

        return $this;
    }

    /**
     * Get prescription
     *
     * @return \AppBundle\Entity\Prescriptions
     */
    public function getPrescription()
    {
        return $this->prescription;
    }
}
