<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
class UserToProductsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
			//->add('product')
            ->add('quantity')
			->add('product', EntityType::class, array(
				'class' => 'AppBundle:Products',
				'query_builder' => function (EntityRepository $er) {
					return $er->createQueryBuilder('u')
						->where('u.status=1')
						->orderBy('u.name','ASC');
				}
			));
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\UserToProducts'
        ));
    }
}
