<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use AppBundle\Entity\DoctorsAddress;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class DoctorsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname')
            ->add('lastname')
            ->add('inami')
            ->add('address', CollectionType::class, array(
                    'entry_type' => DoctorsAddressType::class,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'label' =>false,
                    'entry_options' => ['label' => false]
                )
            )
			->add('status', ChoiceType::class, array(
				'choices'  => array(
					'Yes' => true,
					'No' => false,
				),
				'label' => "Active"
			))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Doctors'
        ));
    }
}
