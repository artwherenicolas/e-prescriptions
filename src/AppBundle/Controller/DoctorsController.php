<?php

namespace AppBundle\Controller;

use AppBundle\Entity\DoctorsAddress;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\View\TwitterBootstrap3View;

use AppBundle\Entity\Doctors;

/**
 * Doctors controller.
 *
 * @Route("/doctors")
 */
class DoctorsController extends Controller
{
    /**
     * Lists all Doctors entities.
     *
     * @Route("/", name="doctors")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository('AppBundle:Doctors')->createQueryBuilder('e');

        list($filterForm, $queryBuilder) = $this->filter($queryBuilder, $request);
        list($doctors, $pagerHtml) = $this->paginator($queryBuilder, $request);
        
        $totalOfRecordsString = $this->getTotalOfRecordsString($queryBuilder, $request);

        return $this->render('doctors/index.html.twig', array(
            'doctors' => $doctors,
            'pagerHtml' => $pagerHtml,
            'filterForm' => $filterForm->createView(),
            'totalOfRecordsString' => $totalOfRecordsString,

        ));
    }

    /**
    * Create filter form and process filter request.
    *
    */
    protected function filter($queryBuilder, Request $request)
    {
        $session = $request->getSession();
        $filterForm = $this->createForm('AppBundle\Form\DoctorsFilterType');

        // Reset filter
        if ($request->get('filter_action') == 'reset') {
            $session->remove('DoctorsControllerFilter');
        }

        // Filter action
        if ($request->get('filter_action') == 'filter') {
            // Bind values from the request
            $filterForm->handleRequest($request);

            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set('DoctorsControllerFilter', $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has('DoctorsControllerFilter')) {
                $filterData = $session->get('DoctorsControllerFilter');
                
                foreach ($filterData as $key => $filter) { //fix for entityFilterType that is loaded from session
                    if (is_object($filter)) {
                        $filterData[$key] = $queryBuilder->getEntityManager()->merge($filter);
                    }
                }
                
                $filterForm = $this->createForm('AppBundle\Form\DoctorsFilterType', $filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }


    /**
    * Get results from paginator and get paginator view.
    *
    */
    protected function paginator($queryBuilder, Request $request)
    {
        //sorting
        $sortCol = $queryBuilder->getRootAlias().'.'.$request->get('pcg_sort_col', 'id');
        $queryBuilder->orderBy($sortCol, $request->get('pcg_sort_order', 'desc'));
        // Paginator
        $adapter = new DoctrineORMAdapter($queryBuilder);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage($request->get('pcg_show' , 10));

        try {
            $pagerfanta->setCurrentPage($request->get('pcg_page', 1));
        } catch (\Pagerfanta\Exception\OutOfRangeCurrentPageException $ex) {
            $pagerfanta->setCurrentPage(1);
        }
        
        $entities = $pagerfanta->getCurrentPageResults();

        // Paginator - route generator
        $me = $this;
        $routeGenerator = function($page) use ($me, $request)
        {
            $requestParams = $request->query->all();
            $requestParams['pcg_page'] = $page;
            return $me->generateUrl('doctors', $requestParams);
        };

        // Paginator - view
        $view = new TwitterBootstrap3View();
        $pagerHtml = $view->render($pagerfanta, $routeGenerator, array(
            'proximity' => 3,
            'prev_message' => 'previous',
            'next_message' => 'next',
        ));

        return array($entities, $pagerHtml);
    }
    
    
    
    /*
     * Calculates the total of records string
     */
    protected function getTotalOfRecordsString($queryBuilder, $request) {
        $totalOfRecords = $queryBuilder->select('COUNT(e.id)')->getQuery()->getSingleScalarResult();
        $show = $request->get('pcg_show', 10);
        $page = $request->get('pcg_page', 1);

        $startRecord = ($show * ($page - 1)) + 1;
        $endRecord = $show * $page;

        if ($endRecord > $totalOfRecords) {
            $endRecord = $totalOfRecords;
        }
        return "Showing $startRecord - $endRecord of $totalOfRecords Records.";
    }

    /**
     * Displays a form to create a new Doctors entity.
     *
     * @Route("/importDoctor", name="doctors_import")
     * @Method({"GET", "POST"})
     */
    public function doctorsImportAction(Request $request) {
		echo 'here';exit;
        set_time_limit(100000000000);
        if (($handle = fopen($_SERVER['DOCUMENT_ROOT'].'/doctors2.csv', "r")) !== FALSE) {
            $cpt=0;
            $em = $this->getDoctrine()->getManager();
            while (($data = fgetcsv($handle, 10000, ",")) !== FALSE) {
                if($cpt>0) {
					print_r($data);exit;
                    $exist = $this->getDoctrine()->getRepository('AppBundle:Doctors')->findOneBy(array('onekey'=>$data[0]));
                    if($exist) {
                       // exit;
						$exist->setInami($data[9]);
						$em->persist($exist);
                        $em->flush();
                    }
                    else {
						echo 'create';exit;
                        $doctor = new Doctors();
                        $address = new DoctorsAddress();
                        $doctor->setFirstname($data[3]);
                        $doctor->setLastname($data[2]);
                        $doctor->setOnekey($data[0]);
                        $doctor->setHospital($data[4]);
                        $doctor->setInami($data[9]);

                        preg_match_all('!\d+!', $data[5], $matches);
                       // print_r($matches[0][0]);exit;
                        //echo end($matches);exit;
                        if(isset($matches[0][0])) {
                            $address->setNumber($matches[0][0]);
                        }
                        else {
                            $address->setNumber(0);
                        }
                        $address->setBox(0);
                        $address->setAddress($data[5]);
                        $address->setCity($data[8]);
                        $address->setZip($data[7]);
                        $address->setDoctor($doctor);

                        if($data[6]=="BE")
                        {
                        }
                        //$address->set
                        $doctor->addAddress($address);

                        $em->persist($doctor);
                        $em->flush();
                    }
                }
                $cpt++;
            }
        }

    }

    /**
     * Displays a form to create a new Doctors entity.
     *
     * @Route("/new", name="doctors_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
    
        $doctor = new Doctors();
        $address = new DoctorsAddress();
        $doctor->addAddress($address);
        $form   = $this->createForm('AppBundle\Form\DoctorsType', $doctor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($doctor);
            $em->flush();
            
            $editLink = $this->generateUrl('doctors_edit', array('id' => $doctor->getId()));
            $this->get('session')->getFlashBag()->add('success', "<a href='$editLink'>New doctor was created successfully.</a>" );
            
            $nextAction=  $request->get('submit') == 'save' ? 'doctors' : 'doctors_new';
            return $this->redirectToRoute($nextAction);
        }
        return $this->render('doctors/new.html.twig', array(
            'doctor' => $doctor,
            'form'   => $form->createView(),
        ));
    }
    

    /**
     * Finds and displays a Doctors entity.
     *
     * @Route("/{id}", name="doctors_show")
     * @Method("GET")
     */
    public function showAction(Doctors $doctor)
    {
        $deleteForm = $this->createDeleteForm($doctor);
        return $this->render('doctors/show.html.twig', array(
            'doctor' => $doctor,
            'delete_form' => $deleteForm->createView(),
        ));
    }
    
    

    /**
     * Displays a form to edit an existing Doctors entity.
     *
     * @Route("/{id}/edit", name="doctors_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Doctors $doctor)
    {
        $deleteForm = $this->createDeleteForm($doctor);
        $editForm = $this->createForm('AppBundle\Form\DoctorsType', $doctor);
        $editForm->handleRequest($request);



        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($doctor);
            $em->flush();
            
            $this->get('session')->getFlashBag()->add('success', 'Edited Successfully!');
            return $this->redirectToRoute('doctors_edit', array('id' => $doctor->getId()));
        }
        return $this->render('doctors/edit.html.twig', array(
            'doctor' => $doctor,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    
    

    /**
     * Deletes a Doctors entity.
     *
     * @Route("/{id}", name="doctors_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Doctors $doctor)
    {
    
        $form = $this->createDeleteForm($doctor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($doctor);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'The Doctors was deleted successfully');
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the Doctors');
        }
        
        return $this->redirectToRoute('doctors');
    }
    
    /**
     * Creates a form to delete a Doctors entity.
     *
     * @param Doctors $doctor The Doctors entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Doctors $doctor)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('doctors_delete', array('id' => $doctor->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
    
    /**
     * Delete Doctors by id
     *
     * @Route("/delete/{id}", name="doctors_by_id_delete")
     * @Method("GET")
     */
    public function deleteByIdAction(Doctors $doctor){
        $em = $this->getDoctrine()->getManager();
        
        try {
            $em->remove($doctor);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'The Doctors was deleted successfully');
        } catch (Exception $ex) {
            $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the Doctors');
        }

        return $this->redirect($this->generateUrl('doctors'));

    }
    

    /**
    * Bulk Action
    * @Route("/bulk-action/", name="doctors_bulk_action")
    * @Method("POST")
    */
    public function bulkAction(Request $request)
    {
        $ids = $request->get("ids", array());
        $action = $request->get("bulk_action", "delete");

        if ($action == "delete") {
            try {
                $em = $this->getDoctrine()->getManager();
                $repository = $em->getRepository('AppBundle:Doctors');

                foreach ($ids as $id) {
                    $doctor = $repository->find($id);
                    $em->remove($doctor);
                    $em->flush();
                }

                $this->get('session')->getFlashBag()->add('success', 'doctors was deleted successfully!');

            } catch (Exception $ex) {
                $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the doctors ');
            }
        }

        return $this->redirect($this->generateUrl('doctors'));
    }
    

}
