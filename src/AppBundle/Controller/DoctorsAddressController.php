<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\View\TwitterBootstrap3View;

use AppBundle\Entity\DoctorsAddress;

/**
 * DoctorsAddress controller.
 *
 * @Route("/doctorsaddress")
 */
class DoctorsAddressController extends Controller
{
    /**
     * Lists all DoctorsAddress entities.
     *
     * @Route("/", name="doctorsaddress")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository('AppBundle:DoctorsAddress')->createQueryBuilder('e');

        list($filterForm, $queryBuilder) = $this->filter($queryBuilder, $request);
        list($doctorsAddresses, $pagerHtml) = $this->paginator($queryBuilder, $request);
        
        $totalOfRecordsString = $this->getTotalOfRecordsString($queryBuilder, $request);

        return $this->render('doctorsaddress/index.html.twig', array(
            'doctorsAddresses' => $doctorsAddresses,
            'pagerHtml' => $pagerHtml,
            'filterForm' => $filterForm->createView(),
            'totalOfRecordsString' => $totalOfRecordsString,

        ));
    }

    /**
    * Create filter form and process filter request.
    *
    */
    protected function filter($queryBuilder, Request $request)
    {
        $session = $request->getSession();
        $filterForm = $this->createForm('AppBundle\Form\DoctorsAddressFilterType');

        // Reset filter
        if ($request->get('filter_action') == 'reset') {
            $session->remove('DoctorsAddressControllerFilter');
        }

        // Filter action
        if ($request->get('filter_action') == 'filter') {
            // Bind values from the request
            $filterForm->handleRequest($request);

            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set('DoctorsAddressControllerFilter', $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has('DoctorsAddressControllerFilter')) {
                $filterData = $session->get('DoctorsAddressControllerFilter');
                
                foreach ($filterData as $key => $filter) { //fix for entityFilterType that is loaded from session
                    if (is_object($filter)) {
                        $filterData[$key] = $queryBuilder->getEntityManager()->merge($filter);
                    }
                }
                
                $filterForm = $this->createForm('AppBundle\Form\DoctorsAddressFilterType', $filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }


    /**
    * Get results from paginator and get paginator view.
    *
    */
    protected function paginator($queryBuilder, Request $request)
    {
        //sorting
        $sortCol = $queryBuilder->getRootAlias().'.'.$request->get('pcg_sort_col', 'id');
        $queryBuilder->orderBy($sortCol, $request->get('pcg_sort_order', 'desc'));
        // Paginator
        $adapter = new DoctrineORMAdapter($queryBuilder);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage($request->get('pcg_show' , 10));

        try {
            $pagerfanta->setCurrentPage($request->get('pcg_page', 1));
        } catch (\Pagerfanta\Exception\OutOfRangeCurrentPageException $ex) {
            $pagerfanta->setCurrentPage(1);
        }
        
        $entities = $pagerfanta->getCurrentPageResults();

        // Paginator - route generator
        $me = $this;
        $routeGenerator = function($page) use ($me, $request)
        {
            $requestParams = $request->query->all();
            $requestParams['pcg_page'] = $page;
            return $me->generateUrl('doctorsaddress', $requestParams);
        };

        // Paginator - view
        $view = new TwitterBootstrap3View();
        $pagerHtml = $view->render($pagerfanta, $routeGenerator, array(
            'proximity' => 3,
            'prev_message' => 'previous',
            'next_message' => 'next',
        ));

        return array($entities, $pagerHtml);
    }
    
    
    
    /*
     * Calculates the total of records string
     */
    protected function getTotalOfRecordsString($queryBuilder, $request) {
        $totalOfRecords = $queryBuilder->select('COUNT(e.id)')->getQuery()->getSingleScalarResult();
        $show = $request->get('pcg_show', 10);
        $page = $request->get('pcg_page', 1);

        $startRecord = ($show * ($page - 1)) + 1;
        $endRecord = $show * $page;

        if ($endRecord > $totalOfRecords) {
            $endRecord = $totalOfRecords;
        }
        return "Showing $startRecord - $endRecord of $totalOfRecords Records.";
    }
    
    

    /**
     * Displays a form to create a new DoctorsAddress entity.
     *
     * @Route("/new", name="doctorsaddress_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
    
        $doctorsAddress = new DoctorsAddress();
        $form   = $this->createForm('AppBundle\Form\DoctorsAddressType', $doctorsAddress);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($doctorsAddress);
            $em->flush();
            
            $editLink = $this->generateUrl('doctorsaddress_edit', array('id' => $doctorsAddress->getId()));
            $this->get('session')->getFlashBag()->add('success', "<a href='$editLink'>New doctorsAddress was created successfully.</a>" );
            
            $nextAction=  $request->get('submit') == 'save' ? 'doctorsaddress' : 'doctorsaddress_new';
            return $this->redirectToRoute($nextAction);
        }
        return $this->render('doctorsaddress/new.html.twig', array(
            'doctorsAddress' => $doctorsAddress,
            'form'   => $form->createView(),
        ));
    }
    

    /**
     * Finds and displays a DoctorsAddress entity.
     *
     * @Route("/{id}", name="doctorsaddress_show")
     * @Method("GET")
     */
    public function showAction(DoctorsAddress $doctorsAddress)
    {
        $deleteForm = $this->createDeleteForm($doctorsAddress);
        return $this->render('doctorsaddress/show.html.twig', array(
            'doctorsAddress' => $doctorsAddress,
            'delete_form' => $deleteForm->createView(),
        ));
    }
    
    

    /**
     * Displays a form to edit an existing DoctorsAddress entity.
     *
     * @Route("/{id}/edit", name="doctorsaddress_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, DoctorsAddress $doctorsAddress)
    {
        $deleteForm = $this->createDeleteForm($doctorsAddress);
        $editForm = $this->createForm('AppBundle\Form\DoctorsAddressType', $doctorsAddress);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($doctorsAddress);
            $em->flush();
            
            $this->get('session')->getFlashBag()->add('success', 'Edited Successfully!');
            return $this->redirectToRoute('doctorsaddress_edit', array('id' => $doctorsAddress->getId()));
        }
        return $this->render('doctorsaddress/edit.html.twig', array(
            'doctorsAddress' => $doctorsAddress,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    
    

    /**
     * Deletes a DoctorsAddress entity.
     *
     * @Route("/{id}", name="doctorsaddress_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, DoctorsAddress $doctorsAddress)
    {
    
        $form = $this->createDeleteForm($doctorsAddress);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($doctorsAddress);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'The DoctorsAddress was deleted successfully');
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the DoctorsAddress');
        }
        
        return $this->redirectToRoute('doctorsaddress');
    }
    
    /**
     * Creates a form to delete a DoctorsAddress entity.
     *
     * @param DoctorsAddress $doctorsAddress The DoctorsAddress entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(DoctorsAddress $doctorsAddress)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('doctorsaddress_delete', array('id' => $doctorsAddress->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
    
    /**
     * Delete DoctorsAddress by id
     *
     * @Route("/delete/{id}", name="doctorsaddress_by_id_delete")
     * @Method("GET")
     */
    public function deleteByIdAction(DoctorsAddress $doctorsAddress){
        $em = $this->getDoctrine()->getManager();
        
        try {
            $em->remove($doctorsAddress);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'The DoctorsAddress was deleted successfully');
        } catch (Exception $ex) {
            $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the DoctorsAddress');
        }

        return $this->redirect($this->generateUrl('doctorsaddress'));

    }
    

    /**
    * Bulk Action
    * @Route("/bulk-action/", name="doctorsaddress_bulk_action")
    * @Method("POST")
    */
    public function bulkAction(Request $request)
    {
        $ids = $request->get("ids", array());
        $action = $request->get("bulk_action", "delete");

        if ($action == "delete") {
            try {
                $em = $this->getDoctrine()->getManager();
                $repository = $em->getRepository('AppBundle:DoctorsAddress');

                foreach ($ids as $id) {
                    $doctorsAddress = $repository->find($id);
                    $em->remove($doctorsAddress);
                    $em->flush();
                }

                $this->get('session')->getFlashBag()->add('success', 'doctorsAddresses was deleted successfully!');

            } catch (Exception $ex) {
                $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the doctorsAddresses ');
            }
        }

        return $this->redirect($this->generateUrl('doctorsaddress'));
    }
    

}
