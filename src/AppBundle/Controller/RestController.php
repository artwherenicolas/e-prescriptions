<?php
namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use AppBundle\Entity\ProductsToPrescriptions;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations\Get; // N'oublons pas d'inclure Get
use AppBundle\Entity\Place;
use FOS\RestBundle\Controller\Annotations\Post; // N'oublons pas d'inclure Get
use FOS\RestBundle\View\ViewHandler;
use FOS\RestBundle\View\View; // Utilisation de la vue de FOSRestBundle
use  AppBundle\Entity\Prescriptions;
use AppBundle\Entity\PrescriptionAddress;
use AppBundle\Entity\OldOrders;
use AppBundle\Entity\Doctors;
use AppBundle\Entity\DoctorsAddress;
use Doctrine\ORM\Query;
use Symfony\Component\Validator\Constraints\DateTime;
class RestController extends FOSRestController
{

    public function X($response) {
        //$response->setHeaders(['Access-Control-Allow-Origin''*']);
       $response->headers->set('Access-Control-Allow-Origin', '*');
        return $response;
    }
	
	/**
	* @Get ("/api/delegate")
	*/
	public function getDelegate() {
		//$delegates = $this->getDoctrine()->getRepository('AppBundle:User')->findAll();
		$em = $this->getDoctrine()->getManager();
		$viewHandler = $this->get('fos_rest.view_handler');
		/*$delegates = 	$result = $em->getRepository("AppBundle:User")->createQueryBuilder('o')
							    ->where('o.roles NOT LIKE :role')
							   ->setParameter('role', "%ROLE_ADMIN%")
							   ->getQuery()
							   ->getResult();*/
							   
							   
		$delegates = $em->getRepository("AppBundle:User")->createQueryBuilder('o')
			->select('o.email','o.id','o.username','o.representativeId')
			//->from('fos_user','o')
			->where('o.roles NOT LIKE :role and o.enabled=1')
			->orderBy('o.email', 'ASC')
			->setParameter('role', "%ROLE_ADMIN%")
			->getQuery()
			->getResult();
				   
		$userManager = $this->container->get('fos_user.user_manager');
		$users = $userManager->findUsers();
		/*foreach($users as $user) {
			$delegates[]= $user;
		}*/
		//$delegates[] = reset($users);
		$view = View::create($delegates);
		
		$view->setFormat('json');
		$view->setHeader('Access-Control-Allow-Origin', '*');
		return $this->x($viewHandler->handle($view));
	}
	
	/**
	* @Get ("/api/delegate/quantity/{delegateId}")
	*/
	public function getDelegateQuantity($delegateId) {
		$return = [];
		$usertoproducts = $this->getDoctrine()->getRepository('AppBundle:UserToProducts')->findBy(['user'=>$delegateId]);
	
		foreach($usertoproducts as $up) {
		
			if($up->getProduct()->getStatus()==1) {
				$return[($up->getProduct()->getId())] = $up->getQuantity();
			}
		}
		
		$viewHandler = $this->get('fos_rest.view_handler');
		
		$view = View::create($return);
		// Création d'une vue FOSRestBundle
		$view->setFormat('json');
		// Gestion de la réponse
		return $this->x($viewHandler->handle($view));
	}
	
	
	/**
	* @Get ("/api/doctor/quantity/{doctorId}")
	*/
	public function getDoctorQuantity(Request $request,$doctorId) {
		
		$doctors = $this->getDoctrine()->getRepository('AppBundle:Doctors')->findBy(['id'=>$doctorId]);
		$prescriptions = $this->getDoctrine()->getRepository('AppBundle:Prescriptions')->findBy(['doctor'=>$doctors]);
		$already=[];
		
		$olds = $this->getDoctrine()->getRepository('AppBundle:OldOrders')->findBy(['doctor'=>$doctors]);
		foreach($olds as $old) {
			//echo $old->getProduct()->getId();exit;
			if($old->getDate()->format('Y')==date('Y')) {
				$already[$old->getProduct()->getId()]=@$already[$old->getProduct()->getId()]+$old->getQuantity();
			}
		}
		
		foreach($prescriptions as $p) {
			$products = $p->getProductToPrescriptions();
			foreach($products as $product) {
				if($p->getDate()->format('Y')==date('Y')) {
					$already[$product->getProduct()->getId()]=@$already[$product->getProduct()->getId()]+$product->getQuantity();
				}
			}
		}
	
		$viewHandler = $this->get('fos_rest.view_handler');
		$success['success'] = 'false';
		$view = View::create($already);
		$view->setFormat('json');
        return $this->x($viewHandler->handle($view));
	}
	
	/**
	* @Get ("/api/update")
	*/
	public function getUpdate(Request $request) {

		$viewHandler = $this->get('fos_rest.view_handler');
		$data = "String to encrypt";
		$ass = file_get_contents($_SERVER['DOCUMENT_ROOT']."/public.pem");
	
		$pubKey = openssl_pkey_get_public($ass);
		
		$encryptedData = "Nicolas";
		openssl_public_encrypt($data, $encryptedData, $pubKey);

		$success['success'] = utf8_encode($encryptedData);
		$view = View::create($success);
		$view->setFormat('json');
        return $this->x($viewHandler->handle($view));
	}
	
	/**
     * @Get("/api/importDoctor")
     */
    public function doctorsImportAction(Request $request) {
		
        set_time_limit(0);
        if (($handle = fopen($_SERVER['DOCUMENT_ROOT'].'/doctors3.csv', "r")) !== FALSE) {
            $cpt=0;
			$cpt2=0;
            $em = $this->getDoctrine()->getManager();
            while (($data = fgetcsv($handle, 23000, ",")) !== FALSE) {
                if($cpt>0 and $data[8]) {
					//echo $data[0];exit;
                    $exist = $this->getDoctrine()->getRepository('AppBundle:Doctors')->findOneBy(array('onekey'=>$data[0]));
					//dump($exist);exit;
					//dump($exist);exit;
                    if($exist) {
					//	echo 'here';exit;
                      // exit;
					   
						/*$exist->setInami($data[8]);
						*/
						$currentAddress = $exist->getAddress()[0];
						$currentAddress->setAddress($data[4]);
                        $currentAddress->setCity($data[6]);
                        $currentAddress->setZip($data[5]);
						$exist->setFirstname($data[1]);
                        $exist->setLastname($data[2]);
						$em->persist($currentAddress);
						$em->persist($exist);
                        if($cpt2>1000) {
							$em->flush();
							$cpt2=0;
						}
                    }
                    else {
						echo 'create';
                        $doctor = new Doctors();
                        $address = new DoctorsAddress();
                        $doctor->setFirstname($data[1]);
                        $doctor->setLastname($data[2]);
                        $doctor->setOnekey($data[0]);
                        $doctor->setHospital($data[3]);
						$doctor->setStatus(1);
                        $doctor->setInami($data[8]);

                        preg_match_all('!\d+!', $data[4], $matches);
                       // print_r($matches[0][0]);exit;
                        //echo end($matches);exit;
                        if(isset($matches[0][0])) {
                            $address->setNumber($matches[0][0]);
                        }
                        else {
                            $address->setNumber(0);
                        }
                        $address->setBox(0);
                        $address->setAddress($data[4]);
                        $address->setCity($data[6]);
                        $address->setZip($data[5]);
                        $address->setDoctor($doctor);

                        if($data[6]=="BE")
                        {
                        }
                        //$address->set
                        $doctor->addAddress($address);

                        $em->persist($doctor);
						if($cpt2>1000) {
							$em->flush();
							$cpt2=0;
						}
					
                    }
                }
				$cpt2++;
                $cpt++;
            }
        }

    }
	
	
	/**
     * @Get("/api/updateDoctorsQuantity")
     */
	public function getUpdateDoctorsQuantityAction(Request $request) {
		set_time_limit(100000000000);
		$em = $this->getDoctrine()->getManager();
        if (($handle = fopen($_SERVER['DOCUMENT_ROOT'].'/Samples2.csv', "r")) !== FALSE) {
            $cpt=0;
            $em = $this->getDoctrine()->getManager();
            while (($data = fgetcsv($handle, 10000, ",")) !== FALSE) {
				if($cpt>0) {
					/**
					* Status = 0
					* Inami = 7
					* Name = 8
					* Quantity = 10
					*/

					
					
				
					
					if($data[1]) {
						//TRANSFORM INAME
						
						$inami = preg_replace("/^(\d{1})(\d{5})(\d{2})(\d{3})$/", "$1-$2-$3-$4",$data[1]);
						

						$doctor = $this->getDoctrine()->getRepository('AppBundle:Doctors')->findOneBy(['inami'=>$inami]);
						/*if($doctor) {
							continue;
						}*/
						if(!$doctor) {
								$result = $em->getRepository("AppBundle:Doctors")->createQueryBuilder('o')
							   ->where('o.inami LIKE :inami')
							   ->setParameter('inami', "%".substr($inami,0,-3)."%")
							   ->getQuery()
							   ->getResult();
							   //dump($result);exit;
						   if(reset($result)) {
							   $doctor=reset($result);
							  //dump($doctor);exit;
						   }
						}
						
						
						
						
						if($doctor) {
							/*echo "<br/>".$cpt.$data[8];echo "<br/>".$cpt.$data[8];echo "<br/>".$cpt.$data[8];echo "<br/>".$cpt.$data[8];
							/**
							* Products Mapping
							*/
							$productsMapping = [
								"SPIOLTO RESPI 2,5MCG/2,5MCG 30" => 11,
								"PRADAXA 150MG 60 HARDE CAP/G S" => 13,
								"JARDIANCE 10MG 30 TABL/CPR ECH" => 19,
								"JARDIANCE 25MG 30 TABL/CPR ECH" => 18,
								"SYNJARDY 60CPR 5MG-1000MG ECHA" => 22,
								"SYNJARDY 60CPR 5MG-850MG ECHAN" => 23,
								"TRAJENTA 5 MG 30 TABL/CPR ECHT" => 12,
								"PRADAXA 110MG 10 HARDE CAP/G S" => 14,
							];
							
							if(isset($productsMapping[$data[8]]) and $data[0]=="Completed") {
								$productId = $productsMapping[$data[8]];
								$product = $this->getDoctrine()->getRepository('AppBundle:Products')->findOneBy(['id'=>$productId]);
								$oldOrder = new oldOrders();
								$oldOrder->setProduct($product);
								$oldOrder->setDoctor($doctor);
								$oldOrder->setQuantity($data[10]);
								$oldOrder->setDate(new \DateTime());
								$em->persist($oldOrder);
								$em->flush();
								unset($oldOrder);
								//echo $productsMapping[$data[8]];
							}
						
						
						
						}
						else {
							//echo substr($inami,0,-3);exit;
						
							echo $inami.'<br/>';
						}
					}
					
					//print_r($data);exit;
					

					
					/**
					* 7 = inami
					* 1 = Quantity
					* 2 = Product Name
					*/
					
					/**
					* SET VARIABLES
					*/
				/*	$inami = $data[7];
					$quantity= $data[1];
					$doctor = $this->getDoctrine()->getRepository('AppBundle:Doctors')->findOneBy(['inami'=>$inami]);
					if($doctor) {
						/**
						* Products Mapping
						*/
					/*	$productsMapping = [
							"Spiolto DOSLI/4ML 1/2,5+2,5MCG +RSP B" => 11,
							"PRADAXA 60 caps 150mg" => 13,
							"Jardiance 10 mg 30 pack" => 19,
							"Synjardy 60cpr 5mg-1000mg" => 22,
							"Synjardy 60cpr 5mg-850mg" => 23,
							"TRAJENTA 30X5mg" => 12,
							"PRADAXA 10 caps 110mg" => 14,
						];
						
						if(isset($productsMapping[$data[2]])) {
							$productId = $productsMapping[$data[2]];
							$product = $this->getDoctrine()->getRepository('AppBundle:Products')->findOneBy(['id'=>$productId]);
							$oldOrder = new oldOrders();
							$oldOrder->setProduct($product);
							$oldOrder->setDoctor($doctor);
							$oldOrder->setQuantity($quantity);
							$oldOrder->setDate(\DateTime::createFromFormat('d/m/Y',$data[0]));
							$em->persist($oldOrder);
							$em->flush();
							unset($oldOrder);
						}
					}
					
					*/
				}
                $cpt++;
            }
        }

    }
	
    /**
     * @Get("/api/products")
     */
    public function getProductsAction(Request $request)
    {
        
		//$products = $this->getDoctrine()->getRepository('AppBundle:Products')->findAll()->getResult();


        //$products = $this->getDoctrine()->getRepository('AppBundle:Products')->ProductsAll();

        $query = $this->getDoctrine()
            ->getRepository('AppBundle:Products')
            ->createQueryBuilder('c')
			->where('c.status = 1')
            ->getQuery();
        $products = $query->getResult(Query::HYDRATE_ARRAY);
        $viewHandler = $this->get('fos_rest.view_handler');

		$view = View::create($products);
        // Création d'une vue FOSRestBundle
		$view->setFormat('json');
        // Gestion de la réponse
        return $this->x($viewHandler->handle($view));
       
    }

    /**
     * @Get("/api/doctors")
     */
    public function getDoctorsAction(Request $request)
    {
		$viewHandler = $this->get('fos_rest.view_handler');
		$filename = "cache_doctor_".date('Y-m-d',time());
		if(@($data=file_get_contents('cache/doctors'.$filename))!==FALSE) {
			
			return $this->x(unserialize($data));
		}
		else {
			$doctors = $this->getDoctrine()->getRepository('AppBundle:Doctors')->findAll();
			$view = View::create($doctors);
			$view->setFormat('json');
			$view->setHeader('Access-Control-Allow-Origin', '*');
			$filename = "cache_doctor_".date('Y-m-d',time());
			file_put_contents('cache/doctors'.$filename,serialize($viewHandler->handle($view)));
		}
		
		
        // Gestion de la réponse
        return $this->x($viewHandler->handle($view));
    }

    /**
     * @Post("/api/prescription/send")
     */
    public function PostPrescriptionAction(Request $request)
    {
        $data = json_decode($request->request->get("data"));
		mail('nd@artwhere.eu',"DEBUG",print_r($data,true));
        $user_manager = $this->get('fos_user.user_manager');
        $em = $this->getDoctrine()->getManager();

		
        $address=$this->getDoctrine()->getRepository('AppBundle:DoctorsAddress')->findOneBy(array('address'=>$data->address));
        $doctor = $this->getDoctrine()->getRepository('AppBundle:Doctors')->findOneBy(array('id'=>$data->doctor->id));
        $reprenstative = $user_manager->findUserByUsername($data->respresentative->username);
		$addressDelivery = new PrescriptionAddress();
        $prescription = new Prescriptions();

        foreach((array)$data->products as $key=>$p) {
            $tmpProduct = $this->getDoctrine()->getRepository('AppBundle:Products')->findOneBy(array('name'=>$p->name,"status"=>1));
			
            $productToPrescription = new ProductsToPrescriptions();
            $productToPrescription->setProduct($tmpProduct);
            $productToPrescription->setPrescription($prescription);
            $productToPrescription->setQuantity($p->quantity);

            $prescription->addProductToPrescription($productToPrescription);
            $em->persist($productToPrescription);
			//$this->updateDelegateQuantity($tmpProduct,$reprenstative,$p->quantity);
			
			
			$quantityDelegate = $this->getDoctrine()->getRepository('AppBundle:UserToProducts')->findOneBy(['user'=>$reprenstative->getId(),'product'=>$tmpProduct->getId()]);
		
			if($quantityDelegate) {
				$quant  = $quantityDelegate->getQuantity();
				$quantityDelegate->setQuantity(($quantityDelegate->getQuantity()-$p->quantity));
				$em->persist($quantityDelegate);
			}
		
			
        }
		
		
		
        $prescription->setDate(new \DateTime());
        $prescription->setDoctor($doctor);
		$prescription->setSignature("");
        $prescription->setRepresentative($reprenstative);
        $prescription->setSchedule(json_encode($data->schedule));
        $prescription->setAddress($address);
		$prescription->setAddressDelivery($addressDelivery);
		$prescription->setDeliveryMode($data->deliveryMode);
		$prescription->setService(0);

		$addressDelivery->setAddress($data->deladdress);
		$addressDelivery->setNumber($data->number);
		$addressDelivery->setZip($data->zip);
		$addressDelivery->setCity($data->city);
		$addressDelivery->setPrescription($prescription);
		
		$em->persist($addressDelivery);
		
		
        $em->persist($prescription);
        $em->flush();
		
        $this->DHLAction($prescription->getId());


        $viewHandler = $this->get('fos_rest.view_handler');
        $success['success'] = 'ok';
        $view = View::create($success);
        $view->setFormat('json');
        $view->setHeader('Access-Control-Allow-Origin', '*');
        return $this->x($viewHandler->handle($view));

    }
	private function updateDelegateQuantity($product,$delegate,$quantity) {
		//$quantityDelegate = $this->getDoctrine()->getRepository('AppBundle:UserToProducts')->findOneBy(['user'=>$delegate->getId(),'product'=>$product->getId()]);
		//$quantityDelegate->setQuantity(($quantityDelegate->getQuantity()-$quantity));
		//
		$em = $this->getDoctrine()->getManager();
		//$em->persist($quantityDelegate);
        $em->flush();
	}
	
	/**
     * @GET("/api/dhlsend/{prescritionId}")
     */
    public function DHLAction($prescritionId)
    {
        $prescrition=$this->getDoctrine()->getRepository('AppBundle:Prescriptions')->findOneBy(array('id'=>$prescritionId));
        //GENERATE XML

        $days = (json_decode($prescrition->getSchedule()));
		
        if($days) {
            foreach($days as $key=>$d) {

                $tempArray = explode('-',$key);
				if(isset($tempArray[3])) {
					$daysDisplay[$tempArray[1]][$tempArray[2]][$tempArray[3]]=1;
				}
				else {
					$daysDisplay[$tempArray[1]][$tempArray[2]]=1;
				}
                
            }
           
        }
	
	
        // dump($prescrition->getProductToDescription());exit;
        $response = new Response();
        $response->headers->set('Content-Type', 'html');
        $response->headers->set('Content-Disposition','attachment; filename="export.xml"');

		//echo $prescrition->getRepresentative()->getEmail();exit;
        $date = date('Y-m-d');
        $time = date('H:i:s');
		$address = $prescrition->getAddressDelivery();
		$address->setAddress(str_replace($address->getNumber(),'',$address->getAddress()));
	
        $render =  $this->render(
            'prescriptions/xml.html.twig',
            array(
                'date' => $date,
                'time' => $time,
                'user' => $prescrition->getRepresentative(),
                'prescription' => $prescrition,
                'doctor' => $prescrition->getDoctor(),
                'products' => $prescrition->getProductToPrescriptions(),
                'days' => $daysDisplay,
				'address'=>$prescrition->getAddressDelivery()
            ),
            $response
        );
		//print_r($prescrition->getAddressDelivery());exit;
        $filename = "MVT000_01_000084_".$date."_".str_replace(':','_',$time)."_".sprintf('%06d',$prescrition->getId())."_".$prescrition->getRepresentative()->getUsername().".xml";
        //echo $_SERVER['DOCUMENT_ROOT'].'/web/xml/trst.xml';exit;
        
		file_put_contents('xml/'.$filename,$render->getContent());
		
		//mail('nd@artwhere.eu','XML COPY','boeqa/in/files/xml/'.$filename);
		
        //FIRST FTP
       /* $conn_id = ftp_connect('ftpqa.pharma-logistics.com');A20180103a$
        $login_result = ftp_login($conn_id, 'boeqa_artware', 'A20180122a$');*/
		
		$conn_id = ftp_connect('ftpprod.pharma-logistics.com');
        $login_result = ftp_login($conn_id, 'boeprod_artware', 'A20180103a$');
		
        $path = $_SERVER['DOCUMENT_ROOT'].'/web/xml/'.$filename;
		/*ftp_pasv($conn_id,true);
       if (ftp_put($conn_id, '/boeprod/in/files/xml/'.$filename, $path, FTP_BINARY)) {
            mail('nd@artwhere.eu','XML COPY','boeqa/in/files/xml/'.$filename);
          //  echo "Le fichier $filename a été chargé avec succès\n";
        } else {
            mail('nd@artwhere.eu','XML COPY FAIL','boeqa/in/files/xml/'.$filename);
           // echo "Il y a eu un problème lors du chargement du fichier \n";
        }*/

       // ftp_close($conn_id);
		$viewHandler = $this->get('fos_rest.view_handler');
		$success['success'] = $error['success'] = 'success';
			$view = View::create($error);
            $view->setFormat('json');
            $view->setHeader('Access-Control-Allow-Origin', '*');
            return $viewHandler->handle($view);
            return $this->x($viewHandler->handle($success));

    }

    /**
     * @Post("/api/login")
     */
    public function LoginAction(Request $request)
    {

        $user_manager = $this->get('fos_user.user_manager');
        $factory = $this->get('security.encoder_factory');

        $user = $user_manager->findUserByUsername($request->request->get("username"));
        $viewHandler = $this->get('fos_rest.view_handler');

        if($user) {
            $encoder = $factory->getEncoder($user);
            $bool = ($user->isEnabled() && $encoder->isPasswordValid($user->getPassword(), $request->request->get("password"), $user->getSalt())) ? true : false;
        }
        else {
            $error['error'] = $error['error'] = 'User not found 22';
            $view = View::create($error);
            $view->setHeader('Access-Control-Allow-Origin', '*');
            $view->setFormat('json');
            return $this->x($viewHandler->handle($view));
        }


        if($bool) {

            $user->setLastLogin(new \DateTime());
            $session = $request->getSession();
            $session->set('user_is_login',$user);

            $view = View::create($user);
            $view->setHeader('Access-Control-Allow-Origin', '*');
            $view->setFormat('json');
            return $this->x($viewHandler->handle($view));

        }
        else

        {
            $error['error'] = $error['error'] = 'User not found';
            $view = View::create($error);
            $view->setFormat('json');
            $view->setHeader('Access-Control-Allow-Origin', '*');
            return $viewHandler->handle($view);
            return $this->x($viewHandler->handle($error));
        }
    }
	
	
	
}