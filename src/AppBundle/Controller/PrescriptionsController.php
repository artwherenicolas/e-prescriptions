<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\View\TwitterBootstrap3View;
use Symfony\Component\HttpFoundation\Response;

use AppBundle\Entity\Prescriptions;

/**
 * Prescriptions controller.
 *
 * @Route("/prescriptions")
 */
class PrescriptionsController extends Controller
{
    /**
     * Lists all Prescriptions entities.
     *
     * @Route("/", name="prescriptions")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
	
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository('AppBundle:Prescriptions')->createQueryBuilder('e');

        list($filterForm, $queryBuilder) = $this->filter($queryBuilder, $request);
        list($prescriptions, $pagerHtml) = $this->paginator($queryBuilder, $request);
        $totalOfRecordsString = $this->getTotalOfRecordsString($queryBuilder, $request);

        return $this->render('prescriptions/index.html.twig', array(
            'prescriptions' => $prescriptions,
            'pagerHtml' => $pagerHtml,
            'filterForm' => $filterForm->createView(),
            'totalOfRecordsString' => $totalOfRecordsString,

        ));
    }

    /**
    * Create filter form and process filter request.
    *
    */
    protected function filter($queryBuilder, Request $request)
    {
        $session = $request->getSession();
        $filterForm = $this->createForm('AppBundle\Form\PrescriptionsFilterType');

        // Reset filter
        if ($request->get('filter_action') == 'reset') {
            $session->remove('PrescriptionsControllerFilter');
        }

        // Filter action
        if ($request->get('filter_action') == 'filter') {
            // Bind values from the request
            $filterForm->handleRequest($request);

            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set('PrescriptionsControllerFilter', $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has('PrescriptionsControllerFilter')) {
                $filterData = $session->get('PrescriptionsControllerFilter');
                
                foreach ($filterData as $key => $filter) { //fix for entityFilterType that is loaded from session
                    if (is_object($filter)) {
                        $filterData[$key] = $queryBuilder->getEntityManager()->merge($filter);
                    }
                }
                
                $filterForm = $this->createForm('AppBundle\Form\PrescriptionsFilterType', $filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }


    /**
    * Get results from paginator and get paginator view.
    *
    */
    protected function paginator($queryBuilder, Request $request)
    {
        //sorting
        $sortCol = $queryBuilder->getRootAlias().'.'.$request->get('pcg_sort_col', 'id');
        $queryBuilder->orderBy($sortCol, $request->get('pcg_sort_order', 'desc'));
        // Paginator
        $adapter = new DoctrineORMAdapter($queryBuilder);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage($request->get('pcg_show' , 20));

        try {
            $pagerfanta->setCurrentPage($request->get('pcg_page', 1));
        } catch (\Pagerfanta\Exception\OutOfRangeCurrentPageException $ex) {
            $pagerfanta->setCurrentPage(1);
        }
        
        $entities = $pagerfanta->getCurrentPageResults();

        // Paginator - route generator
        $me = $this;
        $routeGenerator = function($page) use ($me, $request)
        {
            $requestParams = $request->query->all();
            $requestParams['pcg_page'] = $page;
            return $me->generateUrl('prescriptions', $requestParams);
        };

        // Paginator - view
        $view = new TwitterBootstrap3View();
        $pagerHtml = $view->render($pagerfanta, $routeGenerator, array(
            'proximity' => 3,
            'prev_message' => 'previous',
            'next_message' => 'next',
        ));

        return array($entities, $pagerHtml);
    }
    
    
    
    /*
     * Calculates the total of records string
     */
    protected function getTotalOfRecordsString($queryBuilder, $request) {
        $totalOfRecords = $queryBuilder->select('COUNT(e.id)')->getQuery()->getSingleScalarResult();
        $show = $request->get('pcg_show', 30);
        $page = $request->get('pcg_page', 1);

        $startRecord = ($show * ($page - 1)) + 1;
        $endRecord = $show * $page;

        if ($endRecord > $totalOfRecords) {
            $endRecord = $totalOfRecords;
        }
        return "Showing $startRecord - $endRecord of $totalOfRecords Records.";
    }
    
    

    /**
     * Displays a form to create a new Prescriptions entity.
     *
     * @Route("/new", name="prescriptions_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
    
        $prescription = new Prescriptions();
        $form   = $this->createForm('AppBundle\Form\PrescriptionsType', $prescription);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($prescription);
            $em->flush();
            
            $editLink = $this->generateUrl('prescriptions_edit', array('id' => $prescription->getId()));
            $this->get('session')->getFlashBag()->add('success', "<a href='$editLink'>New prescription was created successfully.</a>" );
            
            $nextAction=  $request->get('submit') == 'save' ? 'prescriptions' : 'prescriptions_new';
            return $this->redirectToRoute($nextAction);
        }
        return $this->render('prescriptions/new.html.twig', array(
            'prescription' => $prescription,
            'form'   => $form->createView(),
        ));
    }
    

    /**
     * Finds and displays a Prescriptions entity.
     *
     * @Route("/{id}", name="prescriptions_show")
     * @Method("GET")
     */
    public function showAction(Prescriptions $prescription)
    {
        $deleteForm = $this->createDeleteForm($prescription);
        return $this->render('prescriptions/show.html.twig', array(
            'prescription' => $prescription,
            'delete_form' => $deleteForm->createView(),
        ));
    }




    /**
     * Displays a form to edit an existing Prescriptions entity.
     *
     * @Route("/{id}/edit", name="prescriptions_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Prescriptions $prescription)
    {
        $deleteForm = $this->createDeleteForm($prescription);
        $editForm = $this->createForm('AppBundle\Form\PrescriptionsType', $prescription);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($prescription);
            $em->flush();
            
            $this->get('session')->getFlashBag()->add('success', 'Edited Successfully!');
            return $this->redirectToRoute('prescriptions_edit', array('id' => $prescription->getId()));
        }
        return $this->render('prescriptions/edit.html.twig', array(
            'prescription' => $prescription,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    
    

    /**
     * Deletes a Prescriptions entity.
     *
     * @Route("/{id}", name="prescriptions_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Prescriptions $prescription)
    {
    
        $form = $this->createDeleteForm($prescription);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($prescription);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'The Prescriptions was deleted successfully');
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the Prescriptions');
        }
        
        return $this->redirectToRoute('prescriptions');
    }
    
    /**
     * Creates a form to delete a Prescriptions entity.
     *
     * @param Prescriptions $prescription The Prescriptions entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Prescriptions $prescription)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('prescriptions_delete', array('id' => $prescription->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }



    
    /**
     * Delete Prescriptions by id
     *
     * @Route("/delete/{id}", name="prescriptions_by_id_delete")
     * @Method("GET")
     */
    public function deleteByIdAction(Prescriptions $prescription){
        $em = $this->getDoctrine()->getManager();
        
        try {
            $em->remove($prescription);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'The Prescriptions was deleted successfully');
        } catch (Exception $ex) {
            $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the Prescriptions');
        }

        return $this->redirect($this->generateUrl('prescriptions'));

    }
    

    /**
    * Bulk Action
    * @Route("/bulk-action/", name="prescriptions_bulk_action")
    * @Method("POST")
    */
    public function bulkAction(Request $request)
    {
        $ids = $request->get("ids", array());
        $action = $request->get("bulk_action", "delete");

        if ($action == "delete") {
            try {
                $em = $this->getDoctrine()->getManager();
                $repository = $em->getRepository('AppBundle:Prescriptions');

                foreach ($ids as $id) {
                    $prescription = $repository->find($id);
                    $em->remove($prescription);
                    $em->flush();
                }

                $this->get('session')->getFlashBag()->add('success', 'prescriptions was deleted successfully!');

            } catch (Exception $ex) {
                $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the prescriptions ');
            }
        }

        return $this->redirect($this->generateUrl('prescriptions'));
    }
    

}
