<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\View\TwitterBootstrap3View;

use AppBundle\Entity\UserToProducts;

/**
 * UserToProducts controller.
 *
 * @Route("/usertoproducts")
 */
class UserToProductsController extends Controller
{
    /**
     * Lists all UserToProducts entities.
     *
     * @Route("/", name="usertoproducts")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository('AppBundle:UserToProducts')->createQueryBuilder('e');

        list($filterForm, $queryBuilder) = $this->filter($queryBuilder, $request);
        list($userToProducts, $pagerHtml) = $this->paginator($queryBuilder, $request);
        
        $totalOfRecordsString = $this->getTotalOfRecordsString($queryBuilder, $request);

        return $this->render('usertoproducts/index.html.twig', array(
            'userToProducts' => $userToProducts,
            'pagerHtml' => $pagerHtml,
            'filterForm' => $filterForm->createView(),
            'totalOfRecordsString' => $totalOfRecordsString,

        ));
    }

    /**
    * Create filter form and process filter request.
    *
    */
    protected function filter($queryBuilder, Request $request)
    {
        $session = $request->getSession();
        $filterForm = $this->createForm('AppBundle\Form\UserToProductsFilterType');

        // Reset filter
        if ($request->get('filter_action') == 'reset') {
            $session->remove('UserToProductsControllerFilter');
        }

        // Filter action
        if ($request->get('filter_action') == 'filter') {
            // Bind values from the request
            $filterForm->handleRequest($request);

            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set('UserToProductsControllerFilter', $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has('UserToProductsControllerFilter')) {
                $filterData = $session->get('UserToProductsControllerFilter');
                
                foreach ($filterData as $key => $filter) { //fix for entityFilterType that is loaded from session
                    if (is_object($filter)) {
                        $filterData[$key] = $queryBuilder->getEntityManager()->merge($filter);
                    }
                }
                
                $filterForm = $this->createForm('AppBundle\Form\UserToProductsFilterType', $filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }


    /**
    * Get results from paginator and get paginator view.
    *
    */
    protected function paginator($queryBuilder, Request $request)
    {
        //sorting
        $sortCol = $queryBuilder->getRootAlias().'.'.$request->get('pcg_sort_col', 'id');
        $queryBuilder->orderBy($sortCol, $request->get('pcg_sort_order', 'desc'));
        // Paginator
        $adapter = new DoctrineORMAdapter($queryBuilder);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage($request->get('pcg_show' , 10));

        try {
            $pagerfanta->setCurrentPage($request->get('pcg_page', 1));
        } catch (\Pagerfanta\Exception\OutOfRangeCurrentPageException $ex) {
            $pagerfanta->setCurrentPage(1);
        }
        
        $entities = $pagerfanta->getCurrentPageResults();

        // Paginator - route generator
        $me = $this;
        $routeGenerator = function($page) use ($me, $request)
        {
            $requestParams = $request->query->all();
            $requestParams['pcg_page'] = $page;
            return $me->generateUrl('usertoproducts', $requestParams);
        };

        // Paginator - view
        $view = new TwitterBootstrap3View();
        $pagerHtml = $view->render($pagerfanta, $routeGenerator, array(
            'proximity' => 3,
            'prev_message' => 'previous',
            'next_message' => 'next',
        ));

        return array($entities, $pagerHtml);
    }
    
    
    
    /*
     * Calculates the total of records string
     */
    protected function getTotalOfRecordsString($queryBuilder, $request) {
        $totalOfRecords = $queryBuilder->select('COUNT(e.id)')->getQuery()->getSingleScalarResult();
        $show = $request->get('pcg_show', 10);
        $page = $request->get('pcg_page', 1);

        $startRecord = ($show * ($page - 1)) + 1;
        $endRecord = $show * $page;

        if ($endRecord > $totalOfRecords) {
            $endRecord = $totalOfRecords;
        }
        return "Showing $startRecord - $endRecord of $totalOfRecords Records.";
    }
    
    

    /**
     * Displays a form to create a new UserToProducts entity.
     *
     * @Route("/new", name="usertoproducts_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
    
        $userToProduct = new UserToProducts();
       // dump($userToProduct);exit;
		//$products = $this->getDoctrine()->getRepository('AppBundle:Products')->findBy(['status'=>1]);
		$form   = $this->createForm('AppBundle\Form\UserToProductsType', $userToProduct);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($userToProduct);
            $em->flush();
            
            $editLink = $this->generateUrl('usertoproducts_edit', array('id' => $userToProduct->getId()));
            $this->get('session')->getFlashBag()->add('success', "<a href='$editLink'>New userToProduct was created successfully.</a>" );
            
            $nextAction=  $request->get('submit') == 'save' ? 'usertoproducts' : 'usertoproducts_new';
            return $this->redirectToRoute($nextAction);
        }
        return $this->render('usertoproducts/new.html.twig', array(
            'userToProduct' => $userToProduct,
            'form'   => $form->createView(),
        ));
    }
    

    /**
     * Finds and displays a UserToProducts entity.
     *
     * @Route("/{id}", name="usertoproducts_show")
     * @Method("GET")
     */
    public function showAction(UserToProducts $userToProduct)
    {
        $deleteForm = $this->createDeleteForm($userToProduct);
        return $this->render('usertoproducts/show.html.twig', array(
            'userToProduct' => $userToProduct,
            'delete_form' => $deleteForm->createView(),
        ));
    }
    
    

    /**
     * Displays a form to edit an existing UserToProducts entity.
     *
     * @Route("/{id}/edit", name="usertoproducts_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, UserToProducts $userToProduct)
    {
        $deleteForm = $this->createDeleteForm($userToProduct);
        $editForm = $this->createForm('AppBundle\Form\UserToProductsType', $userToProduct);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($userToProduct);
            $em->flush();
            
            $this->get('session')->getFlashBag()->add('success', 'Edited Successfully!');
            return $this->redirectToRoute('usertoproducts_edit', array('id' => $userToProduct->getId()));
        }
        return $this->render('usertoproducts/edit.html.twig', array(
            'userToProduct' => $userToProduct,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    
    

    /**
     * Deletes a UserToProducts entity.
     *
     * @Route("/{id}", name="usertoproducts_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, UserToProducts $userToProduct)
    {
    
        $form = $this->createDeleteForm($userToProduct);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($userToProduct);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'The UserToProducts was deleted successfully');
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the UserToProducts');
        }
        
        return $this->redirectToRoute('usertoproducts');
    }
    
    /**
     * Creates a form to delete a UserToProducts entity.
     *
     * @param UserToProducts $userToProduct The UserToProducts entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(UserToProducts $userToProduct)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('usertoproducts_delete', array('id' => $userToProduct->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
	
    
    /**
     * Delete UserToProducts by id
     *
     * @Route("/delete/{id}", name="usertoproducts_by_id_delete")
     * @Method("GET")
     */
    public function deleteByIdAction(UserToProducts $userToProduct){
        $em = $this->getDoctrine()->getManager();
        
        try {
            $em->remove($userToProduct);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'The UserToProducts was deleted successfully');
        } catch (Exception $ex) {
            $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the UserToProducts');
        }

        return $this->redirect($this->generateUrl('usertoproducts'));

    }
    

    /**
    * Bulk Action
    * @Route("/bulk-action/", name="usertoproducts_bulk_action")
    * @Method("POST")
    */
    public function bulkAction(Request $request)
    {
        $ids = $request->get("ids", array());
        $action = $request->get("bulk_action", "delete");

        if ($action == "delete") {
            try {
                $em = $this->getDoctrine()->getManager();
                $repository = $em->getRepository('AppBundle:UserToProducts');

                foreach ($ids as $id) {
                    $userToProduct = $repository->find($id);
                    $em->remove($userToProduct);
                    $em->flush();
                }

                $this->get('session')->getFlashBag()->add('success', 'userToProducts was deleted successfully!');

            } catch (Exception $ex) {
                $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the userToProducts ');
            }
        }

        return $this->redirect($this->generateUrl('usertoproducts'));
    }
    

}
