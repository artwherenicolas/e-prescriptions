<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Pagerfanta\Pagerfanta;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\View\TwitterBootstrap3View;

use AppBundle\Entity\Products;
use AppBundle\Entity\UserToProducts;
/**
 * Products controller.
 *
 * @Route("/products")
 */
class ProductsController extends Controller
{
	
	/**
     * Displays a form to create a new Doctors entity.
     *
     * @Route("/import/quantiy4", name="doctors_import_quantities")
     * @Method("GET")
     */
    public function quantityImportAction(Request $request) {
		
		$em = $this->getDoctrine()->getManager();
        set_time_limit(100000000000);
		$products = $this->getDoctrine()->getRepository('AppBundle:Products')->findAll();
		if (($handle = fopen($_SERVER['DOCUMENT_ROOT'].'/quantity4.csv', "r")) !== FALSE) {
            $cpt=0;
            $em = $this->getDoctrine()->getManager();
            while (($data = fgetcsv($handle, 10000, ",")) !== FALSE) {
				//print_r($data);exit;
				//nathalie.ameel@boehringer-ingelheim.com
				$name = strtolower(trim($data[4]).'.'.$data[3]);
				$mail = $name.'@boehringer-ingelheim.com';
				$user = $this->getDoctrine()->getRepository('AppBundle:User')->findOneBy(['email'=>$mail]);
				if(!$user) {
					$name = strtolower(trim($data[4]).'.'.str_replace(' ','_',$data[3]));
					$mail = $name.'@boehringer-ingelheim.com';
					$user = $this->getDoctrine()->getRepository('AppBundle:User')->findOneBy(['email'=>$mail]);

				}
				if(!$user) {
					$name = strtolower(trim($data[4]).'.'.str_replace(' ','-',$data[3]));
					$mail = $name.'@boehringer-ingelheim.com';
					$user = $this->getDoctrine()->getRepository('AppBundle:User')->findOneBy(['email'=>$mail]);

				}
				if($user) {
					foreach($products as $p) {
						//echo $p->getId().'<br/>';exit;
						$userToProduct = $this->getDoctrine()->getRepository('AppBundle:UserToProducts')->findOneBy(['user'=>$user, 'product'=>$p]);
						if($userToProduct) {
						
						}
						else {
							$userToProduct = new UserToProducts();
							$userToProduct->setUser($user);
							$userToProduct->setProduct($p);
							$userToProduct->setQuantity(0);
							
						}
						switch($p->getId()) {
							case 22 : $userToProduct->setQuantity($data[8]);
								break;
							case 11 : $userToProduct->setQuantity($data[12]);
								break;
							case 12 : $userToProduct->setQuantity($data[7]);
								break;
							case 13 : $userToProduct->setQuantity($data[11]);
								break;
							case 14 : $userToProduct->setQuantity($data[10]);
								break;
							case 18 : $userToProduct->setQuantity($data[6]);
								break;
							case 24 : $userToProduct->setQuantity($data[5]);
								break;
							case 23 : $userToProduct->setQuantity($data[9]);
								break;
						}
						$em->persist($userToProduct);
						$em->flush();
					}
					
				}
				//print_R($data);exit;
            }
        }

    }
	
	
    /**
     * Lists all Products entities.
     *
     * @Route("/", name="products")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository('AppBundle:Products')->createQueryBuilder('e');

        list($filterForm, $queryBuilder) = $this->filter($queryBuilder, $request);
        list($products, $pagerHtml) = $this->paginator($queryBuilder, $request);
        
        $totalOfRecordsString = $this->getTotalOfRecordsString($queryBuilder, $request);

        return $this->render('products/index.html.twig', array(
            'products' => $products,
            'pagerHtml' => $pagerHtml,
            'filterForm' => $filterForm->createView(),
            'totalOfRecordsString' => $totalOfRecordsString,

        ));
    }

    /**
    * Create filter form and process filter request.
    *
    */
    protected function filter($queryBuilder, Request $request)
    {
        $session = $request->getSession();
        $filterForm = $this->createForm('AppBundle\Form\ProductsFilterType');

        // Reset filter
        if ($request->get('filter_action') == 'reset') {
            $session->remove('ProductsControllerFilter');
        }

        // Filter action
        if ($request->get('filter_action') == 'filter') {
            // Bind values from the request
            $filterForm->handleRequest($request);

            if ($filterForm->isValid()) {
                // Build the query from the given form object
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
                // Save filter to session
                $filterData = $filterForm->getData();
                $session->set('ProductsControllerFilter', $filterData);
            }
        } else {
            // Get filter from session
            if ($session->has('ProductsControllerFilter')) {
                $filterData = $session->get('ProductsControllerFilter');
                
                foreach ($filterData as $key => $filter) { //fix for entityFilterType that is loaded from session
                    if (is_object($filter)) {
                        $filterData[$key] = $queryBuilder->getEntityManager()->merge($filter);
                    }
                }
                
                $filterForm = $this->createForm('AppBundle\Form\ProductsFilterType', $filterData);
                $this->get('lexik_form_filter.query_builder_updater')->addFilterConditions($filterForm, $queryBuilder);
            }
        }

        return array($filterForm, $queryBuilder);
    }


    /**
    * Get results from paginator and get paginator view.
    *
    */
    protected function paginator($queryBuilder, Request $request)
    {
        //sorting
        $sortCol = $queryBuilder->getRootAlias().'.'.$request->get('pcg_sort_col', 'id');
        $queryBuilder->orderBy($sortCol, $request->get('pcg_sort_order', 'desc'));
        // Paginator
        $adapter = new DoctrineORMAdapter($queryBuilder);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage($request->get('pcg_show' , 10));

        try {
            $pagerfanta->setCurrentPage($request->get('pcg_page', 1));
        } catch (\Pagerfanta\Exception\OutOfRangeCurrentPageException $ex) {
            $pagerfanta->setCurrentPage(1);
        }
        
        $entities = $pagerfanta->getCurrentPageResults();

        // Paginator - route generator
        $me = $this;
        $routeGenerator = function($page) use ($me, $request)
        {
            $requestParams = $request->query->all();
            $requestParams['pcg_page'] = $page;
            return $me->generateUrl('products', $requestParams);
        };

        // Paginator - view
        $view = new TwitterBootstrap3View();
        $pagerHtml = $view->render($pagerfanta, $routeGenerator, array(
            'proximity' => 3,
            'prev_message' => 'previous',
            'next_message' => 'next',
        ));

        return array($entities, $pagerHtml);
    }
    
    
    
    /*
     * Calculates the total of records string
     */
    protected function getTotalOfRecordsString($queryBuilder, $request) {
        $totalOfRecords = $queryBuilder->select('COUNT(e.id)')->getQuery()->getSingleScalarResult();
        $show = $request->get('pcg_show', 10);
        $page = $request->get('pcg_page', 1);

        $startRecord = ($show * ($page - 1)) + 1;
        $endRecord = $show * $page;

        if ($endRecord > $totalOfRecords) {
            $endRecord = $totalOfRecords;
        }
        return "Showing $startRecord - $endRecord of $totalOfRecords Records.";
    }
    
    

    /**
     * Displays a form to create a new Products entity.
     *
     * @Route("/new", name="products_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
    
        $product = new Products();
        $form   = $this->createForm('AppBundle\Form\ProductsType', $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
           	$userManager = $this->get('fos_user.user_manager');
			$em->persist($product);
            $em->flush();
			$users = $userManager->findUsers();
			foreach($users as $u) {
				$productToUser = new UserToProducts();
				$productToUser->setProduct($product);
				$productToUser->setUser($u);
				$productToUser->setQuantity(0);
				$em->persist($productToUser);
				$em->flush();
			}
            
            $editLink = $this->generateUrl('products_edit', array('id' => $product->getId()));
            $this->get('session')->getFlashBag()->add('success', "<a href='$editLink'>New product was created successfully.</a>" );
			
		
			
			
			
            $nextAction=  $request->get('submit') == 'save' ? 'products' : 'products_new';
            return $this->redirectToRoute($nextAction);
        }
		
        return $this->render('products/new.html.twig', array(
            'product' => $product,
            'form'   => $form->createView(),
        ));
    }
    

    /**
     * Finds and displays a Products entity.
     *
     * @Route("/{id}", name="products_show",requirements={"page"="\d+"})
     * @Method("GET")
     */
    public function showAction(Products $product)
    {
        $deleteForm = $this->createDeleteForm($product);
        return $this->render('products/show.html.twig', array(
            'product' => $product,
            'delete_form' => $deleteForm->createView(),
        ));
    }
    
    

    /**
     * Displays a form to edit an existing Products entity.
     *
     * @Route("/{id}/edit", name="products_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Products $product)
    {
        $deleteForm = $this->createDeleteForm($product);
        $editForm = $this->createForm('AppBundle\Form\ProductsType', $product);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();
            
            $this->get('session')->getFlashBag()->add('success', 'Edited Successfully!');
            return $this->redirectToRoute('products_edit', array('id' => $product->getId()));
        }
        return $this->render('products/edit.html.twig', array(
            'product' => $product,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    
    

    /**
     * Deletes a Products entity.
     *
     * @Route("/{id}", name="products_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Products $product)
    {
    
        $form = $this->createDeleteForm($product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($product);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'The Products was deleted successfully');
        } else {
            $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the Products');
        }
        
        return $this->redirectToRoute('products');
    }
    
    /**
     * Creates a form to delete a Products entity.
     *
     * @param Products $product The Products entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Products $product)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('products_delete', array('id' => $product->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
    
    /**
     * Delete Products by id
     *
     * @Route("/delete/{id}", name="products_by_id_delete")
     * @Method("GET")
     */
    public function deleteByIdAction(Products $product){
        $em = $this->getDoctrine()->getManager();
        
        try {
            $em->remove($product);
            $em->flush();
            $this->get('session')->getFlashBag()->add('success', 'The Products was deleted successfully');
        } catch (Exception $ex) {
            $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the Products');
        }

        return $this->redirect($this->generateUrl('products'));

    }
    

    /**
    * Bulk Action
    * @Route("/bulk-action/", name="products_bulk_action")
    * @Method("POST")
    */
    public function bulkAction(Request $request)
    {
        $ids = $request->get("ids", array());
        $action = $request->get("bulk_action", "delete");

        if ($action == "delete") {
            try {
                $em = $this->getDoctrine()->getManager();
                $repository = $em->getRepository('AppBundle:Products');

                foreach ($ids as $id) {
                    $product = $repository->find($id);
                    $em->remove($product);
                    $em->flush();
                }

                $this->get('session')->getFlashBag()->add('success', 'products was deleted successfully!');

            } catch (Exception $ex) {
                $this->get('session')->getFlashBag()->add('error', 'Problem with deletion of the products ');
            }
        }

        return $this->redirect($this->generateUrl('products'));
    }
    

}
