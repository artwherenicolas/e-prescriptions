<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ProductsRepository extends EntityRepository
{

    public function ProductsAll() {
        $query = $this->getDoctrine()
            ->getRepository('AppBundle:Products')
            ->createQueryBuilder('c')
            ->getQuery();
        $products = $query->getResult(Query::HYDRATE_ARRAY);
        return $products;
    }

}

?>