Login.logout = function() {
		//Clearr localStorage
		swal({
		  title: 'Are you sure?',
		  text: "You will be redirect to login page",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes'
		},
		function(isConfirm) {
		  if (isConfirm) {
			Storage.s.deleteKey('currentUser');
			Router.go("Login!init");
			Tools.truncateDatabase();
		  }
		
		});
}
