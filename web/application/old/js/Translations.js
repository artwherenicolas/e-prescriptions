Translations = {
	language : "fr",
	"getWord": function($in) {
		return Translations.words[Translations.language][$in];

	},
	"words": {
		"fr": {
			"Coordonnées de l\'utilisateur":"Coordonnées de l\'utilisateur",
			"Demande écrite d\'échantillons":"Demande écrite d\'échantillons",
			"Prescripteur":"Prescripteur",
			"Nom":"Nom",
			"Rue":"Rue",
			"Ville":"Ville",
			"Code postal":"Code postal",
			"Prenom":"Prenom",
			"Adresse de livraison":"Adresse de livraison",
			"Numero INAMI":"Numero INAMI",
			"Echantillons":"Echantillons",
			"Quantite":"Quantite",
			
			"Aller à l\' étape":"Aller à l\' étape"
		},
		"en": {
			"Nom":"Name",
		},
		"nl": {
			"Demande écrite d\'échantillons":"Schriftelijke aanvraag voor monsters",
			"Prescripteur":"Voorschrijver",
			"Nom":"Naam",
			"Rue":"Straat",
			"Ville":"Stad",
			"Code postal":"PostalCode",
			"Prenom":"Voornaam",
			"Adresse de livraison":"Leveringsadres",
			"Numero INAMI":"Riziv nummer",
			"Echantillons":"Monsters",
			"Quantite":"Aantal",
		
			"Aller à l\' étape":"Ga naar stap"
		}
	},
}


$ = jQuery.noConflict();
$(document)
	.ready(function(){


	})
	.on('click','#lang li',function(){
		if(!$(this).is('.active')){
			$('#lang li').removeClass('active');
			$(this).addClass('active');
			Translations.language = $(this).data('lang');
		}
	});