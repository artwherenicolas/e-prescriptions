var Tools = {
    "ajax_stack": {},
    "errors":{
        "Doctrine\DBAL\Exception\UniqueConstraintViolationException":"You can't do this, this unique record already exists",
        "overlap_zones":"This zip range overlaps an existing one",
        "invalid_zone":"The start ZIP must be lower than the end ZIP",
        "unexisting_apb":"Unexisting APB"
    },
    "init":function() {

    },
    "setRedCheckLink":function(link) {
        $('.navbar-brand.change_page').attr('data-href',link);
    },
    "convertHex": function(hex,opacity){
        hex = hex.replace('#','');
        r = parseInt(hex.substring(0,2), 16);
        g = parseInt(hex.substring(2,4), 16);
        b = parseInt(hex.substring(4,6), 16);
        result = 'rgba('+r+','+g+','+b+','+opacity/100+')';
        return result;
    },
    "openNextTask":function($task_id,$assignment_id) {
        var $taskPanelSelector = $("#task_panel_"+$task_id+"_"+$assignment_id);

        if($taskPanelSelector.nextAll('div:first').is('.task_panel')) {

            $taskPanelSelector.removeClass('active');
            $('li[data-task="'+$task_id+'"]').addClass('todo-done');
            var $task = $taskPanelSelector.nextAll('.task_panel:first').children().children().attr('data-task');

            $('li[data-task="'+$task+'"]').addClass('active');
            $('li[data-task="'+$task_id+'"]').next('li').trigger('click');
           // $taskPanelSelector.nextAll('.task_panel:first').addClass('active');
        }
    },
    "sAjax": function (method,url,data,callback,fallback,file) {

        var akey = url+"-"+JSON.stringify(data);

        if (Tools.ajax_stack[akey]==1) {
            return true;
        }
        var timeout = 2000;
        if(method=="POST") {
            timeout=15000;
        }
        if(method=="GET") {
            $('.ajax').show();
        }
        Tools.ajax_stack[akey] = 1;
        console.log(data);
        //$('.modal-footer').hide();
        $ajax = {
            url: App.endpoint+url,
            method: method,
            data: data,
            dataType: "json",
            timeout: timeout,
            success: function(msg) {
                delete(Tools.ajax_stack[akey]);
                callback(msg);
                $('#modalNoConnection').modal('toggle');
                $('.ajax').hide();
                Storage.set('ajax_fail',{});
                clearTimeout(Tools.ajax_timer);
                $('.modal-footer').show();
            },
            error : function(result, status, error){

                delete(Tools.ajax_stack[akey]);
                Tools.showModalNoInternet();
                if(method=="GET") {
                    Tools.showModalNoInternet();
                    Tools.sAjax(method,url,data,callback);
                    //setTimeout(function() {Router.route() },1000);

                }
                else {
                    //Save in local storage.
                    Tools.showModalNoInternet();
                    Storage.set('ajax_fail',{data:data,url:url,method:method,callback:callback});
                    Tools.sAjax(method,url,data,callback);
                }
            }
        };

        if (typeof file != "undefined") {
            $.extend($ajax,{
                cache: false,
                processData: false,
                contentType: false
            });
        }

        return $.ajax($ajax);
    },
    "showModalNoInternet":function() {

        $('.modal').not('#modalNoConnection').modal('hide');
        if($('#modalNoConnection').length==0) {
            $('#modalContainer').append(nunjucks.render('modals/modalNoConnection.twig', {}));
            $('#modalNoConnection').modal('show');
        }
    },
    "count":function (myObject) {
        if (typeof myObject != "object") return 0;
        return Object.keys(myObject).length;
    },
    "ucfirst":function(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    },
    "chunk":function(arr, len) {

        var chunks = [],
            i = 0,
            n = arr.length;

        while (i < n) {
            chunks.push(arr.slice(i, i += len));
        }

        return chunks;
    },
    "getById":function($obj,id) {
        for(var x=0;x<Tools.count($obj);x++) {
            if ($obj[x].id == id) return $obj[x];
        }
    },
    "extractIds":function($obj) {
        var tmp = [];
        for(var x=0;x<Tools.count($obj);x++) {
            tmp.push($obj.id);
        }
        return tmp;
    },
    "dict":function($obj) {
        $tmp = {};
        for(var i in $obj) {
            $tmp[$obj[i]["id"]] = $obj[i]["label"];
        }
        return $tmp;
    },
    "now":function() {
        return (new Date()).getTime();
    },
    "nospace":function($t) {
        return $t.replace(/ /gi,"");
    },
    "isEmptyField":function($field) {

        if($field.val()=="none" ) {
            $field.addClass('error');
            return true;
        }
        else if(typeof $field.val()=="undefined")
        {
            $('.pharmacyList').addClass('error');
            return true;
        }
        else {
           return false;
        }
    },
    "setResumeValue":function($data,$subtask,$type,$assignment){
        var $temp = $data.split(',');
        if($type=="boolean" && $temp[0]==0) {
            if($temp[$temp.length-1]=="") {
                var reason = $temp[$temp.length-2];
            }
            else {
                var reason = $temp[$temp.length-1];
            }
            
            var $output = '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>';
            $('.resume-subtask-id-'+$subtask+'-'+$assignment).html($output);
            $('.resume-subtask-id-'+$subtask+'-'+$assignment).after("<br><p class='reason'>Reason : "+reason+"</p>");

        } else {
            var $output = '<span class="glyphicon glyphicon-ok" aria-hidden="true"></span>';
            $('.resume-subtask-id-'+$subtask+'-'+$assignment).html($output);
        }
    },
    "scrollList":function() {
        setTimeout(function(e) {
            $("#pharmacie-list").animate({ scrollTop: $('#pharmacie-list li.todo-done').position().top-$('#pharmacie-list li:first').position().top }, 2000);}
            ,1000
        )
    },
    "setInputValue":function($data,$subtask,$type,$id){
        switch($type) {
            case "boolean" :
                    var value = $data.split(',');
                    if(isNaN($data)) {
                        $('.input-subtask-' + $subtask + '-' + value[0]).prop('checked', true).trigger('change');
                    }
                    var $selector = $('.subtask-wrapper-'+$subtask);
                    $selector.find('.btn').removeClass('active');
                    if($data==0) var $d="false";
                    else var $d="true";
                    if($d=="false" || ($data!=0 && $data!=1)) {

                        $selector.find('a[data-value="false"]').addClass('active');
                        $('.input-bool-reason-'+$subtask).parent().parent().removeClass('hidden');

                        $('.input-bool-reason-'+$subtask+" [value='"+value[1]+"']").prop('selected', true);
                        $('.input-subtask-'+$subtask+'-false').prop('checked', true);
                        if(value[2]) {
                            $('.input-subtask-bool-reason-text-'+$subtask).removeClass('hidden');
                            $('.input-subtask-bool-reason-text-'+$subtask).val(value[2]);
                        }
                    }
                    else {
                        $('.input-bool-reason-'+$subtask).val('');
                        $('.input-subtask-'+$subtask+'-false').prop('checked', true);
                        $('.input-bool-reason-'+$subtask).parent().addClass('hidden');
                        $selector.find('a[data-value="true"]').addClass('active');
                    }

                break;
            case 'radios': case 'list' : case 'checkboxes' :
                    var $arrayData = $data.split(',');
                    for(i=0;i<$arrayData.length;i++) {
                        var $element = $('.input-subtask-' + $subtask + '[value="' + $arrayData[i] + '"]');
                        $element.prop('checked', true);
                    }
                break;
            case 'text' :
                    var $element = $('.input-subtask-' + $subtask);
                    $element.html($data);
                break;
            case 'picture' :
                $('.input-subtask-'+$subtask).after("<img class='picture-task-"+$subtask+"' style='width: 100%' src='http://redpharma.artwhere.net/pictures/"+$id+"/"+$data+"' />");
                break;
            case 'number' :
                console.log('.input-subtask-' + $subtask);
                var $element = $('.input-subtask-' + $subtask);
                $element.val($data);
                break;
        }

    }
};




jQuery.fn.serializeObject = function() {
    var arrayData, objectData;
    arrayData = this.serializeArray();
    objectData = {};

    $.each(arrayData, function() {
        var value;

        if (this.value != null) {
            value = this.value;
        } else {
            value = '';
        }

        if (objectData[this.name] != null) {
            if (!objectData[this.name].push) {
                objectData[this.name] = [objectData[this.name]];
            }

            objectData[this.name].push(value);
        } else {
            objectData[this.name] = value;
        }
    });

    return objectData;
};
