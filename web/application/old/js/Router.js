Router = {
    "controller": "",
    "method": "",
    "addclass": "",
    "history": new Array(),
    "param1": "",
    "param2": "",
    "init": function () {
        $(window).on('hashchange', function () {
            Router.route();
        });
        Router.route();
    },
    "go": function (path) {
    /*    swal.close();*/
        var old_hash = document.location.hash;
        var new_hash = "#!" + path;
        document.location.hash = "#!" + path;
        if (old_hash == new_hash) Router.route();
    },
    "route": function () {
     /*   $('.modal').modal('hide');*/

        if (document.location.hash == "") {
            Router.go("Login!init");
            return true;
        }

        $url = document.location.hash.split("!");

        if (typeof window[$url[1]] != "undefined") {
            if (typeof window[$url[1]][$url[2]] == 'function') {
                Router.controller = $url[1];
                Router.method = $url[2];
                Router.params = $url.slice(3,1000);
                window[$url[1]][$url[2]]();
            }
        }
        
    },
    "_r":function(page, params,callback) {

        $.extend(params, {
            "endpoint": App.endpoint
        });

        var $inactive_page = $(".page.inactive");
        
        var $active_page = $(".page.active");
        

        $inactive_page.html(nunjucks.render(page + '.twig', params));

        var file = page.replace("/","-");

        $active_page.removeClass("active").addClass("active");
        $inactive_page.removeClass("inactive");

        $inactive_page.attr("id",file);
        $active_page.removeAttr("id");
        $("#"+file).addClass("active");

        $active_page.addClass("inactive").removeClass("active");
        $inactive_page.addClass("active").removeClass("inactive");



        setTimeout(function() {
            $active_page.html("");
        },0);

        if (callback) callback();
        $("#ajax").hide();
        $('.ajax').hide();

    },
}