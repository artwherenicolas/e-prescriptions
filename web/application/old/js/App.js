App = {
    "statuses":[
        "New",
        "Done",
        "Free repass asked",
        "Free repass accepted",
        "Free repass done",
        "Paying repass asked",
        "Paying repass accepted",
        "Paying repass done",
        "Repass refused",
        "Closed",
        "Refused"
    ],
    "endpoint":"https://redpharma.artwhere.net/api/",
    "init":function() {
        Storage.init();
        App.env = nunjucks.configure('templates', {
            web: {useCache: true},
            autoescape: true
        });
        App.env.addFilter('count', function ($obj) {
            return Tools.count($obj);
        });
        App.env.addFilter('countByXpath', function ($obj,$key,$value) {
            var $cnt = 0;
            for(var i in $obj) {
                if ($obj[i][$key]==$value) {
                    $cnt += 1;
                }
            }
            return $cnt;
        });
        App.env.addFilter('formatComment',function($pharmacie,$assignmentId,$taskId) {
            return jsonPath(Pharmacies.current,"$.assignment[?(@.id=="+$assignmentId+")].task_action[?(@.task_id=="+$taskId+")].comment")[0];
        });
        App.env.addFilter('checkTask',function($assignment) {
            console.log($assignment);
        });
        App.env.addFilter('isDone',function($task,$assignmentId) {
            var isMandatory = true;
            var exist =  jsonPath(Pharmacies.current,"$.assignment[?(@.id=="+$assignmentId+")].task_action[?(@.task_id=="+$task.id+")]").length;
            if(exist==1){
                return true;
            }
            else {
                return false;
            }
        });
        App.env.addFilter('findStatus',function($assignment,$assignmentId,$taskId) {
            return jsonPath($assignment,"$.assignment[?(@.id=="+$assignmentId+")].task_action[?(@.task_id=="+$taskId+")].status")[0];
        });

        App.env.addFilter('findValue',function($obj,$taksId) {
            console.log($obj);
        });        
		App.env.addFilter('t', function ($obj) {
            return Translations.getWord($obj);
        });
        App.env.addFilter('getById', function ($obj, id) {
            return Tools.getById($obj, id);
        });
        App.env.addFilter('date_time', function (str) {
            if (typeof str == "undefined") return "";
            return moment(str).format("DD-MM-YYYY");
        });
        App.env.addFilter('hour', function (str) {
            if (typeof str == "undefined") return "";
            return moment(str).format("HH:mm");
        });
        App.env.addFilter('mins', function (str) {
            if (typeof str == "undefined") return "";
            return moment(str).unix()/60;
        });
        App.env.addFilter("day_name", function(id) {
            var days = ["M","T","W","T","F","S","S"];
            return days[id];
        });
        App.env.addFilter('date_time_year', function (str) {
            if (typeof str == "undefined") return "";
            return moment(str).format("YYYY");
        });
        App.env.addFilter('color',function(id) {
            return ["#5bc0de","#5cb85c","#f0ad4e","#d9534f","#3d315b","#444b6e","#708b75","#9ab87a","#f8f991"][id];
        });
        App.env.addFilter('get_pic',function(json) {
            var json = JSON.parse(json);
            return json[0];
        });
        App.env.addFilter('duration',function(start,end) {
            var diff = moment(end).format("x")-moment(start).format("x");
            return diff;
        });
        App.env.addFilter('duration_format',function(duration) {
            var duration = moment.duration(duration);
            return moment.utc(duration.as('milliseconds')).format('HH:mm:ss')
        });

        App.env.addFilter('get_status',function(status) {
            return App.statuses[status];
        });

        App.env.addFilter('json',function(json) {
            var json = JSON.parse(json);
            return json;
        });

        App.env.addFilter('icon_type',function(str) {
            switch(str) {
                case "boolean":
                    return "fa-toggle-on";
                case "number":
                    return "fa-hashtag";
                case "checkboxes":
                    return "fa-check-square";
                case "radios":
                    return "fa-dot-circle-o";
                case "list":
                    return "fa-list";
                case "picture":
                    return "fa-picture-o";
                case "text":
                    return "fa-font";
            }
        });
        App.events();
        Router.init();

    },
    "datepicker": function() {
        $("[xtype='date']").each(function() {
            new Pikaday({
                field: this,
                format: 'YYYY-MM-DD'
            });
            $(this).prop("readonly","readonly");
        });
    },
    "multiselect":function() {
        $('SELECT[multiple]').multiselect();
    },
    "events": function() {

        $(document)
            //Pharmacies filter
            .on('keyup','#pharmacie-search-input',function(){
                if($(this).val().length==0) {
                    $('#pharmacie-list li').show();
                }

                if($(this).val().length>3) {
                    var keyword = $(this).val();
                    $('#pharmacie-list li').each(function(e) {
                        if($(this).attr('data-title').toLowerCase().indexOf(keyword.toLowerCase()) > -1) {
                            $(this).show();
                        }
                        else {
                            $(this).hide();
                        }
                    })
                }
            })

            .on("click", "A", function(e) {
               // if(Keyboard.isVisible) { Keyboard.hide(); }
                if ($(this).attr("href") == document.location.hash) {
                    Router.route();
                }
                if ($(this).attr("href") == "#" || $(this).attr("href") == "") {
                    e.preventDefault();
                    if($(this).attr('data-target')) {
                        /*if(Keyboard.isVisible) {
                            Keyboard.hide(); 
                        }                            
                        else {*/
                            var target=$(this).attr('data-target');
                            setTimeout(function() {$(target).modal('show');}, 200)
                            //}
                        
                    }
                    
                }
            })
            .on('click','.goto',function(e) {
                if (!$(e.target).is(":checkbox")) {
                    Router.go($(this).data("href"));
                }
            })
            .on("click",".pharma_closed",function(e){
                e.preventDefault();
                var select = ($(this).parents(".row").find('select').val('00:00'));
            })
            .on("click", ".submit",function(e) {
                $(".submit I").remove();
                $(this).append("<i class='fa fa-check'></i>");
                setTimeout(function() {
                    $(".submit I").remove();
                },1000);
            })
            .on("change",".file_form [type='file']",function() {
                $(this).parents("form").submit();
            })
            .on("click", ".action",function(e) {
                e.preventDefault();
                eval($(this).data("action"));
            })
            .on('click',".documentBtn",function(e){
            e.preventDefault();
            window.open("https://docs.google.com/viewer?url="+encodeURIComponent($(this).data('href')),"_blank","location=yes");
        });
    },
    "reload":function() {
        document.location.href = "index.html";
    }
}


$(document).ready(function() {
	    App.init();
    FastClick.attach(document.body);
    window.addEventListener('load', function() {
                            new FastClick(document.body);
                            }, false);
});

document.addEventListener("deviceready", onDeviceReady, false);

function onDeviceReady() {
    App.init();
    FastClick.attach(document.body);
    window.addEventListener('load', function() {
                            new FastClick(document.body);
                            }, false);
}
