var Login = {
    "init":function() {
		Translations.init();
		//Tools.checkUpdate();
		if(typeof Storage.get('currentUserLog')!= "undefined") {
			
				var date1 = Storage.get('lastLogin');
				var date2 = Date.now();
				
				var hours = Math.abs(date1 - date2) / 36e5;
				if(hours>4) {
					Router._r("login/login",{},function() {});
					return;					
				}
			
				Login.load(Storage.get('currentUserLog'));
				return;
		}
		Router._r("login/login",{username:Storage.get('username')},function() {});
    },
	"load" : function(msg) {
		$('.ajax').show();
		Storage.set('currentUserLog',msg);
		Storage.set('lastLogin',Date.now());
		Prescription.representative = msg;
		$('.ajax').hide();
		Router.go("Prescription!selectDelegate");
	},
    "login":function() {
        Tools.sAjax("POST","api/login",{"username" :$('input[name="id"]').val(), password:$('input[name="pin"]').val()},
            function(msg) {
                if(msg.error) {
                    swal ( "Oops" ,  Translations.getWord("Login error") ,  "error" )
                    return;
                }
				Storage.set('username',$('input[name="id"]').val());
				Login.load(msg);
            },
            function(){
                swal ( "Oops" ,  "No internet" ,  "error" )
            }
        );

    },
    "logout":function() {
		//Clearr localStorage
		swal({
		  title: 'Are you sure?',
		  text: "You will be redirect to login page",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Yes'
		},
		function(isConfirm) {
		  if (isConfirm) {
			Storage.s.deleteKey('currentUser');
			Router.go("Login!init");
		  }
		
		});
    }
}