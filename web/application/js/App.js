App = {
    "endpoint":"http://dev.e-prescription.artwhere.net/web/",
    "init":function() {
        Storage.init();

        App.env = nunjucks.configure('templates', {
            web: {useCache: true},
            autoescape: true
        });
        App.env.addFilter('count', function ($obj) {
            return Tools.count($obj);
        });
        App.env.addFilter('countByXpath', function ($obj,$key,$value) {
            var $cnt = 0;
            for(var i in $obj) {
                if ($obj[i][$key]==$value) {
                    $cnt += 1;
                }
            }
            return $cnt;
        });       
		App.env.addFilter('t', function ($obj) {
            return Translations.getWord($obj);
        });
        App.env.addFilter('getById', function ($obj, id) {
            return Tools.getById($obj, id);
        });
        App.env.addFilter('date_time', function (str) {
            if (typeof str == "undefined") return "";
            return moment(str).format("DD-MM-YYYY");
        });
        App.env.addFilter('hour', function (str) {
            if (typeof str == "undefined") return "";
            return moment(str).format("HH:mm");
        });
        App.env.addFilter('mins', function (str) {
            if (typeof str == "undefined") return "";
            return moment(str).unix()/60;
        });
        App.env.addFilter("day_name", function(id) {
            var days = ["M","T","W","T","F","S","S"];
            return days[id];
        });
        App.env.addFilter('date_time_year', function (str) {
            if (typeof str == "undefined") return "";
            return moment(str).format("YYYY");
        });
        App.env.addFilter('color',function(id) {
            return ["#5bc0de","#5cb85c","#f0ad4e","#d9534f","#3d315b","#444b6e","#708b75","#9ab87a","#f8f991"][id];
        });
        App.env.addFilter('get_pic',function(json) {
            var json = JSON.parse(json);
            return json[0];
        });
        App.env.addFilter('duration',function(start,end) {
            var diff = moment(end).format("x")-moment(start).format("x");
            return diff;
        });
        App.env.addFilter('duration_format',function(duration) {
            var duration = moment.duration(duration);
            return moment.utc(duration.as('milliseconds')).format('HH:mm:ss')
        });



        App.env.addFilter('json',function(json) {
            var json = JSON.parse(json);
            return json;
        });

        App.events();
        Router.init();

    },
    "datepicker": function() {
        $("[xtype='date']").each(function() {
            new Pikaday({
                field: this,
                format: 'YYYY-MM-DD'
            });
            $(this).prop("readonly","readonly");
        });
    },
    "multiselect":function() {
        $('SELECT[multiple]').multiselect();
    },
    "events": function() {

        $(document)
  

            .on("click", "A", function(e) {
               // if(Keyboard.isVisible) { Keyboard.hide(); }
                if ($(this).attr("href") == document.location.hash) {
                    Router.route();
                }
                if ($(this).attr("href") == "#" || $(this).attr("href") == "") {
                    e.preventDefault();
                    if($(this).attr('data-target')) {
                        /*if(Keyboard.isVisible) {
                            Keyboard.hide(); 
                        }                            
                        else {*/
                            var target=$(this).attr('data-target');
                            setTimeout(function() {$(target).modal('show');}, 200)
                            //}
                        
                    }
                    
                }
            })
			.on('click','.selectAll',function(e) {
				e.preventDefault();
				$('.regular_schedule td').each(function() {
					$(this).trigger('click');
				});
			})
			.on('click','.sf-btn-cancel',function(e) {
				
				swal({
					type:'success',
					title: 'Are you sure?',
					confirmButtonText: 'Confirm',
					showCloseButton: true,
					showCancelButton: true,
					cancelButtonText:'Close',
					/*onOpen: () => {
						swal.showLoading();
					}*/
				},
				
				function(isConfirm) {
				  if (isConfirm) {
					App.reload();
				  }
				
				});
				
			})
			.on('change','#delegateChoice',function(e) {
				Prescription.setDelegate($(this).val());
			})
			.on('change','#checkBox',function(e) {
				if(this.checked) {
					Prescription.deliveryMode=1;
					$('.serviceWrapper').show();
					$('.regular_schedule').hide();
					$('.advanced_schedule').show();
				}
				else {
					Prescription.deliveryMode=0;
					Prescription.service=null;
					$('.serviceWrapper').hide();
					$('.regular_schedule').show();
					$('.advanced_schedule').hide();
				}
			})
            .on('click','.goto',function(e) {
                if (!$(e.target).is(":checkbox")) {
                    Router.go($(this).data("href"));
                }
            })
            .on("click",".pharma_closed",function(e){
                e.preventDefault();
                var select = ($(this).parents(".row").find('select').val('00:00'));
            })
            .on("click", ".submit",function(e) {
                $(".submit I").remove();
                $(this).append("<i class='fa fa-check'></i>");
                setTimeout(function() {
                    $(".submit I").remove();
                },1000);
            })
            .on("change",".file_form [type='file']",function() {
                $(this).parents("form").submit();
            })
            .on("click", ".action",function(e) {
                e.preventDefault();
                eval($(this).data("action"));
            })
			.on('click','.btn.editAdresse',function(e) {
				e.preventDefault();
					jQuery('.row.editAdresse').toggle();
				})
            .on('click',".documentBtn",function(e){
            e.preventDefault();
            window.open("https://docs.google.com/viewer?url="+encodeURIComponent($(this).data('href')),"_b^k","location=yes");
        });
    },
    "reload":function() {
        document.location.href = "index.html";
    }
}

$(document).ready(function() {
	App.init();
});

document.addEventListener("deviceready", onDeviceReady, false);

function onDeviceReady() {
    App.init();
    /*FastClick.attach(document.body);
    window.addEventListener('load', function() {
                            new FastClick(document.body);
                            }, false);*/
}
