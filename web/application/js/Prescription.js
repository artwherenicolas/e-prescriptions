var Prescription = {
	"doctor":null,
	"products":{},
	"schedule":{},
	"street":null,
	"city":null,
	"number":null,
	"zip":null,
	"slider":null,
	"representative":null,
	"deladdress":null,
	"service":null,
	"deliveryMode":0,
	"signature":null,
	"resume":function() {
	},
	
	"setResume":function() {
		var products = Products.get();
		var productsSelected = Prescription.products;
		var content = nunjucks.render('prescription/resume.twig', { 
			products:productsSelected,
			representative:Prescription.representative
			})
		$('#resume').html(content);
	},
	"submit":function(){
		//var $signature = $(".signature");
		Prescription.signature= Storage.get('currentUserLog').username;
		Prescription.deladdress=$('input[name="adresse_del"]').val();
		Prescription.number=$('input[name="number_del"]').val();
		Prescription.zip=$('input[name="postal_del"]').val();
		Prescription.city=$('input[name="city_del"]').val();
		Prescription.service=$('input[name="service"]').val();
		
		var dataToSend = {
			"respresentative" :Prescription.representative,
			"products":Prescription.products,
			"address":Prescription.address,
			"doctor":Prescription.doctor,
			"signature":Prescription.signature,
			"schedule":Prescription.schedule,
			"service":Prescription.service,
			"deladdress":Prescription.deladdress,
			"number":Prescription.number,
			"zip":Prescription.zip,
			"city":Prescription.city,
			"deliveryMode":Prescription.deliveryMode
		};
		
		Tools.sAjax("POST","api/prescription/send",{ data:JSON.stringify(dataToSend) },
			function(msg) {
				
				swal({
					type:'success',
					title: 'Your order is done',
					confirmButtonText: 'Confirm',
					/*onOpen: () => {
						swal.showLoading();
					}*/
				},
				
				function(isConfirm) {
				  if (isConfirm) {
					App.reload();
				  }
				
				});
				
			},
			function(){
				alert('FALSE');
			}
		);
	},
	"send":function() {
		swal({
					type:'warning',
					title: 'Are you sure?',
					confirmButtonText: 'Confirm',
					cancelButtonText: 'Cancel',
					 showCancelButton: true,
					 buttons: true,
					/*onOpen: () => {
						swal.showLoading();
					}*/
				},
				
				function(isConfirm) {
				  if (isConfirm) {
					Prescription.submit();
				  }
				
				});

	},
	"selectDelegate":function() {
		Tools.sAjax("GET","api/delegate",{},
							
							function(msg) {
								Storage.set('delegates',msg);
								Router._r("prescription/selectDelegate",{
									delegate:msg
								},function() {
					
								});
							}
						);
		
	},
	"setDelegate":function(delegateId) {
		var delegates=Storage.get('delegates');
		for(var i=0;i<=delegates.length;i++) {
			console.log(i);
			if(delegateId==delegates[i].id) {
				Prescription.representative = delegates[i];
				Storage.set('currentUser',delegates[i]);
				Doctor.init();
			}
		}
		
	},
    "step1":function() {
			//var products = JSON.stringify(Products.get());


			Router._r("prescription/step1",{
				products: Storage.get("products"),
				user: Storage.get('currentUser'),
			},
			function() {
				//Doctor.setAutocomplete();
				if(Storage.get('currentUser')) {
					Prescription.representative = Storage.get('currentUser');
				}
				if(window.innerHeight < window.innerWidth){
					setTimeout(function(){
							var contHeight = window.innerHeight-(330);
							if(Prescription.detectmob() == true){
								$("#wizard").stepFormWizard({
									transition: 'slide',
									stepEffect:'flip',
									duration: 800,
									height: contHeight,
									showNav: 'left',
								});
							}else{
								Prescription.slider = $("#wizard").stepFormWizard({
									transition: 'slide',
									duration: 800,
									height: contHeight,
									onNext: function(i, wizard) {
										if(i==0) {
											Products.quantity();
										}
										if(i==2) {
											Prescription.setResume();
										}
									},
									onFinish: function() {
										$("#wizard").on('sf-finish', function(e, from, data) {
											e.preventDefault(); // this you have to call if you need to interrupt submit form
											Prescription.comment=jQuery('textarea[name="comment"]').val();
											Prescription.send();
											return;
										})
										
									}
								});
								$('.sf-controls').append('<a class="next-btn sf-right sf-btn sf-btn-cancel" href="#" style="display: inline;margin-right:5px">CANCEL</a>');
								Prescription.slider.activeNext(false);
								
							}
							$(".sf-step").mCustomScrollbar({
					            theme: "dark-3",
					            scrollButtons: {
					                enable: true
					            }
					        });
					},300);
				}else{
					alert('Turn your phone');
				}
				$(".sf-step").mCustomScrollbar({
							theme: "dark-3",
							scrollButtons: {
								enable: true
							}
						});
				$(document)
				.on('click','.check-day',function(){
					if($(this).is('.active')){
						$(this).removeClass('active');
					}else{
						$(this).addClass('active');
					}
				})
				.on('click','.schedule td',function() {
					if($(this).hasClass('active')) {
						//ADD
						Prescription.schedule[$(this).attr('data-day')] =1
					}
					else {
						//Remove
						delete Prescription.schedule[$(this).attr('data-day')];
					}
				
				})
				.on('change','.advanced_schedule_input',function() {
					var currentday = $(this).attr('data-day');
					var currentStatus = $(this).is(':checked')
					$('input[data-day='+currentday+']').prop('checked', false);
					console.log(currentStatus);
					if(currentStatus) {
						$(this).prop('checked', true);
					}
					else {
						$(this).prop('checked', false);
					}
					
					delete Prescription.schedule[$(this).attr('data-day')+'-1'];
					delete Prescription.schedule[$(this).attr('data-day')+'-2'];
					
					if($(this).is(':checked')) {
						Prescription.schedule[$(this).attr('data-day')+'-'+$(this).attr('data-subperiod')]=1;
					}
					
					
					
				
				})
				.on('change','.product-quantity',function(){

					Prescription.products[$(this).attr('data-product')] =
						{
							productId: $(this).attr('data-product'),
							quantity: $(this).val(),
							name:$(this).attr('data-name'),
						}



				});
			});
    },
	"step2":function() {
			Router._r("prescription/step2",{
		},function() {

		});
    },
	"step3":function() {
			Router._r("prescription/step3",{
		},function() {

		});
    },
	"thanks":function() {
			Router._r("prescription/thanks",{
		},function() {

		});
    },
    "logout" : function(){
    	alert('You\'ve been logged off ');
    },
    "detectmob" : function() { 
	 if( navigator.userAgent.match(/Android/i)
	 || navigator.userAgent.match(/iPhone/i)
	 || navigator.userAgent.match(/iPod/i)
	 || navigator.userAgent.match(/BlackBerry/i)
	 || navigator.userAgent.match(/Windows Phone/i)
	 ){
	    return true;
	  }
	 else {
	    return false;
	  }
	}
}