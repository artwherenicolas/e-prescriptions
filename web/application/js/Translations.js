Translations = {
	"language" : "fr",
	"init":function() {
		if(Storage.get('language')) {
			Translations.language=Storage.get('language');
			$('#lang li').removeClass('active');
		$('li[data-lang="'+Storage.get('language')+'"]').addClass('active');
		}		
	},
	
	"getWord": function($in) {
		return Translations.words[Translations.language][$in];

	},
	"set":function(language) {
		Translations.language=language;
		$('#lang li').removeClass('active');
		$('li[data-lang="'+language+'"]').addClass('active');
		Storage.set('language',language);
		App.reload();
	},
	"words": {
		"fr": {
			"Valider":"Valider",
			"Recapitulatif de votre commande":"Récapitulatif de votre commande",
			"Selection des echantillons":"Séléction des échantillons",
			"Connecté en tant que":"Connecté en tant que",
			"Déconnexion":"Déconnexion",
			"Signature":"Signature",
			"Coordonnées de l\'utilisateur":"Coordonnées du médecin",
			"Demande écrite d\'échantillons":"Demande écrite d\'échantillons",
			"Prescripteur":"Rep",
			"Nom":"Nom",
			"Commentaire":"Commentaire",
			"Rue":"Rue",
			"Ville":"Ville",
			"Code postal":"Code postal",
			"Prenom":"Prenom",
			"Adresse de livraison":"Adresse de livraison",
			"lundi":"lundi",
			"mardi":"mardi",
			"mercredi":"mercredi",
			"jeudi":"jeudi",
			"vendredi":"vendredi",
			"Numero INAMI":"INAMI",
			"Echantillons":"Echantillons",
			"Quantite":"Quantités",
			"SKU":"SKU",
			"Login error":"Mauvais login/password",
			"Aller à l\' étape":"Aller à l\' étape",
			"Livraison":"Livraison",
			"N°":"N°",
			"Bte":"Bte",
			"Service":"Service",
			"service specifique":"Demande spécifique du docteur",
			"Modifier l'adresse de livraison":"Modifier l'adresse de livraison",
		},
		"en": {
			"Nom":"Name",
		},
		"nl": {
			"Service":"Service",
			"Livraison":"Levering",
			"Valider":"Bevestigen",
			"Selection des echantillons":"Keuze van stalen",
			"Connecté en tant que":"Verbonden als",
			"Déconnexion":"Afmelden",
			"SKU":"SKU",
			"Modifier l'adresse de livraison":"Verander het afleveradres",
			"Signature":"Handtekening",
			"Demande écrite d\'échantillons":"Schriftelijke aanvraag voor stalen",
			"Prescripteur":"Rep",
			"Nom":"Naam",
			"Rue":"Straat",
			"lundi":"Maandag",
			"mardi":"Dinsdag",
			"N°":"Nr",
			"Bte":"Box",
			"service specifique":"Dienst aanvraag van de arts",
			"Recapitulatif de votre commande":"Samenvatting van de bestelling",
			"mercredi":"Woensdag",
			"jeudi":"Donderdag",
			"vendredi":"Vrijdag",
			"Ville":"Stad",
			"Code postal":"Postcode",
			"Prenom":"Voornaam",
			"Commentaire":"Commentaire",
			"Adresse de livraison":"Leveringsadres",
			"Numero INAMI":"RIZIV",
			"Echantillons":"Stalen",
			"Quantite":"Aantal",
			"Login error":"Verkeerd wachtwoord of inloggen",
			"Aller à l\' étape":"Ga naar stap",
			"Coordonnées de l\'utilisateur":"Gegevens van de arts"
		}
	},
}
