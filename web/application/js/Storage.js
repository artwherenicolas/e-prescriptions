var Storage = {
    "s": null,
    "init": function() {
        Storage.s = simpleStorage;
    },
    "dump": function() {
        console.log(JSON.parse(localStorage.getItem("simpleStorage")));
    },
    "get": function(key) {
        return Storage.s.get(key);
    },
    "set": function(key,val,options) {
        Storage.s.set(key,val,options);
    },
    "update_current":function($key,$val) {
        var $current = Storage.get_current();
        if (typeof $current == "undefined") {
            $current = {};
        }
        $current[$key] = $val;
        Storage.set("rc.current",$current);
    },
    "get_current": function() {
        return Storage.get("rc.current");
    },
    "action":function($key,$val) {
        var $actions = Storage.get("rc.actions");
        if (typeof $actions == "undefined") {
            $actions = {};
        }
        $actions[$key] = $val;
        Storage.set("rc.actions",$actions);
    },
    "timer":function($key,$val) {
        var $timers = Storage.get("rc.timers");
        if (typeof $timers == "undefined") {
            $timers = {};
        }
        $timers[$key] = $val;
        Storage.set("rc.timers",$timers);
    },
    "get_timer":function($key) {
        if (typeof Storage.get("rc.timers") == "undefined") {
            return 0;
        }
        if (typeof Storage.get("rc.timers")[$key] == "undefined") {
            return 0;
        }
        return Storage.get("rc.timers")[$key];
    }

};