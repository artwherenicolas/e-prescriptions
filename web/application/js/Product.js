var Products = {
    "init":function() {
		Products.getDelegateQuantity();
		if(typeof Storage.get("products")=="undefined") {
			Tools.sAjax("GET","api/products",{},
				function(msg) {
					Storage.set("products",msg);
					Router.go("Prescription!step1");
				}
			);
		}
		else {
			Router.go("Prescription!step1");
		}
    },
	"getDelegateQuantity":function() {
			var currentUser = Storage.get('currentUser');
			Tools.sAjax("GET","api/delegate/quantity/"+currentUser.id,{},
				function(msg) {
					Storage.set('delegateQuantity',msg);
					$('.ajax').hide();
				},
				function(){
					//alert('THERE S AN UPDATE');
				}
		)
		
	},
	"quantity":function() {
	
			Tools.sAjax("GET","api/doctor/quantity/"+Doctor.current.id,{},
				function(msg) {
					Storage.set("quantity",msg);
					var products = Products.get();
					var content = nunjucks.render('includes/products.twig', { 
							products:products,
							quantityDelegate:Storage.get('delegateQuantity'),
							quantity:msg
						})
						setTimeout(function() {
							$('.alternate.products').html(content);
							$('.ajax').hide();
						},1000);

				}

			);

	},
    "get":function() {
        return Storage.get("products");
    }
}