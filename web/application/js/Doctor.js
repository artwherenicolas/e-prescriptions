var Doctor = {
	"list":null,
    "current":null,
	"init":function() {
		Tools.sAjax("GET","api/doctors",{},
			function(msg) {
				Doctor.list=msg;
				//Storage.set("doctors",msg);
				//Tools.insertDatabase(Tools.encodeData(JSON.stringify(msg)));
				console.log(JSON.stringify(msg[0]));
				setTimeout(function() {
					Doctor.setAutocomplete(Doctor.list);
				},4000);
				Products.init();
				
			}
		);
		
	},
    "setAutocomplete":function(doctors) {
		
        var doctorsAutocomplete= new Array();
		var doctorsAutocompleteOnkey= new Array();
        for(i in doctors) {
			
            doctorsAutocomplete.push({
                label : doctors[i].firstname+' '+doctors[i].lastname,
                value: i
            });
			
			doctorsAutocomplete.push({
                label : doctors[i].inami,
                value: i
            });
            
        }
		
        var input = document.getElementById("doctorAutocomplete");
		var inputOnekey = document.getElementById("onekeyAutocomplete");
        
		new Awesomplete(input, {
            list:doctorsAutocomplete
        });
		
		new Awesomplete(inputOnekey, {
            list:doctorsAutocomplete
        });

        $('#doctorAutocomplete,#onekeyAutocomplete').on('awesomplete-selectcomplete',function(e){
			
            var doctorData = (doctors[e.originalEvent.text.value]);
            Doctor.current=doctorData;
            Prescription.doctor=doctorData;
            $('input[name="nom"]').val(doctorData.lastname);
            $('input[name="prenom"]').val(doctorData.firstname);
            $('input[name="inami"]').val(doctorData.inami);
	
            if(Doctor.current.address.length>1) {
                Doctor.selectAddress();
            }
            if(Doctor.current.address.length==1) {
                Prescription.address = doctorData.address[0].address;
                Doctor.setAddress(doctorData.address[0]);
            }
        });

    },
	"setAddress":function(address) {
		$('input[name="adresse"]').val(address.address)
		$('input[name="box"]').val(address.box);
		$('input[name="number"]').val(address.number);
		$('input[name="postal"]').val(address.zip);
		$('input[name="city"]').val(address.city);
		$('input[name="adresse_del"]').val(address.address)
		$('input[name="box_del"]').val(address.box);
		$('input[name="number_del"]').val(address.number);
		$('input[name="postal_del"]').val(address.zip);
		$('input[name="city_del"]').val(address.city);
		Prescription.slider.activeNext(true);
	},
    "selectAddress":function() {
        var address = Doctor.current.address;
        var tmp =new Array();
        for(var i in address) {
            tmp[i] = address[i].address+" "+address[i].address+" "+address[i].number+" "+address[i].zip +" "+address[i].city;
            Prescription.address = address[i].address;
        }

        swal({
            title: 'Select address',
            input: 'select',
            inputOptions: tmp,
            inputPlaceholder: 'Select address',
            showCancelButton: true,
        }).then(function (result) {
			Doctor.setAddress(Doctor.current.address[result.value]);
        })
    }
}